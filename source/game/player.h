#ifndef GAME_PLAYER_H
#define GAME_PLAYER_H

#include "header.h"

class Player
{
private:
	Player(){};

public:
	int money;

	static Player& GetInstance();

	void Init();
};

#endif