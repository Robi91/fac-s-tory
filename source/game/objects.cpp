#ifndef GAME_OBJECTS_CPP
#define GAME_OBJECTS_CPP

#include "objects.h"

//=========================================================
// BUTTON
//=========================================================

Button::Button(V2 pos) : Object()
{
	type = BUTTON;
	use = false;
	o = NULL;
	message = USE;
	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("gui");
	sprite->position = V3(pos.x, pos.y, 0.01f);
	sprite->size = V2(0.08f, 0.08f);
	sprite->uv = V4(0.25f, 0.0, 0.34375f, 0.09375f);
	SpriteManager::GetInstance().Add(sprite);
}

void Button::Reset()
{
	use = false;
	sprite->uv = V4(0.25f, 0.0, 0.34375f, 0.09375f);
}

void Button::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case USE:
		use = true;
		sprite->uv = V4(0.25f, 0.09375f, 0.34375f, 0.1875f);
		if(o != NULL)
		{
			o->Input(0, message, 0, dt);
		}
		break;

	case SETO1:
		o = (Object*)data;
		break;
	}
}

void Button::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
}

//=========================================================
// SWITCH
//=========================================================

Switch::Switch(V2 pos) : Button(pos)
{
	type = SWITCH;
	sprite->uv = V4(0.34375f, 0.0, 0.4365234375f, 0.09375f);
}

void Switch::Update(float dt)
{
	if(use && o != NULL) o->Input(0, message, 0, dt);
}

void Switch::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case USE:
		if(use)
		{
			use = false;
			sprite->uv = V4(0.34375f, 0, 0.4365234375f, 0.09375f);
		}else{
			use = true;
			sprite->uv = V4(0.34375f, 0.09375f, 0.4365234375f, 0.1875f);
		}
		break;
	case SETO1:
		o = (Object*)data;
		printf("Dodano obiekt do polaczenia\n");
		break;
	}
}

void Switch::Clear()
{
	Button::Clear();
}
//=========================================================
// PUSZKA
//=========================================================
Can::Can(V2 pos) : Package()
{
	type = PACKAGE_CAN;
	painting = 0.0f;
	maxPainting = 1.0f;

	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("package");
	sprite->position = V3(pos.x, pos.y, -0.000001f);
	sprite->size = V2(0.07f, 0.07f);
	sprite->uv = V4(0.125f, 0.0, 0.25f, 0.125f);
	SpriteManager::GetInstance().Add(sprite);

	sprite2 = new Sprite();
	sprite2->texture = TextureManager::GetInstance().GetTexture("package");
	sprite2->position = V3(pos.x, pos.y, 0.000001f);
	sprite2->size = V2(0.07f, 0.07f);
	sprite2->uv = V4(0.0, 0.0, 0.125f, 0.125f);
	sprite2->color = V4(1.0f,1.0f, 1.0f, 0.0f);
	SpriteManager::GetInstance().Add(sprite2);
	
	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixture;

	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2(pos.x, pos.y);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	
	polygonShape.SetAsBox(0.028f, 0.06f);
	fixture.shape = &polygonShape;
	fixture.density = 1.0f;
	
	body->CreateFixture(&fixture);
}

void Can::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
	SpriteManager::GetInstance().Remove(sprite2);
	PhysicsManager::GetInstance().world->DestroyBody(body);
}

void Can::Update(float dt)
{
	sprite->position = V3(body->GetPosition().x, body->GetPosition().y, 0);
	sprite->angle = Func::RtoD(body->GetAngle());
	sprite2->position = V3(body->GetPosition().x, body->GetPosition().y, 0);
	sprite2->angle = Func::RtoD(body->GetAngle());
}

void Can::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case PAINT: 
		painting += *(float*)data * dt;
		sprite2->color.w = painting;
		sprite->color.w = 1.5f - painting;
		break;
	}
}


//=========================================================
// KREATOR PUSZEK
//=========================================================
CanCreator::CanCreator(V2 pos) : Object()
{
	speed = 0.2f;
	prevCreate = 0;

	type = PACKAGECREATOR_CAN;

	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("package");
	sprite->position = V3(pos.x, pos.y, 0.0000001f);
	sprite->size = V2(0.1f, 0.2f);
	sprite->uv = V4(0.25f, 0.0, 0.375f, 0.25f);
	SpriteManager::GetInstance().Add(sprite);
	
	sprite2 = new Sprite();
	sprite2->texture = TextureManager::GetInstance().GetTexture("package");
	sprite2->position = V3(pos.x, pos.y, -0.0000001f);
	sprite2->size = V2(0.1f, 0.2f);
	sprite2->uv = V4(0.375f, 0.0, 0.5f, 0.25f);
	SpriteManager::GetInstance().Add(sprite2);
}

void CanCreator::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case USE: //Tworzenie puszki
		if( prevCreate > 0 ) return;
		prevCreate = speed;
		V2 pos;
		pos.x = sprite->position.x;
		pos.y = sprite->position.y;
		Can* c = new Can(pos);
		ObjectManager::GetInstance().Add(c);
		break;
	}
}

void CanCreator::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
	SpriteManager::GetInstance().Remove(sprite2);
}


//=========================================================
// LICZNIK
//=========================================================
Timer::Timer() : Object()
{
	type = TIMER;
	o = NULL;
	time = 0.0f;
	delay = 2.0f;
	play = false;
	stopAndPlay = false;
	message = USE;
}

void Timer::Update(float dt)
{
	if(play == false ) return;
	time += dt;
	if(time > delay)
	{
		if(o != NULL)
		{
			o->Input(this, message, 0, dt);
		}
		time = fmod(time, delay);
	}
	if(stopAndPlay) play = false;
}

void Timer::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case ON:
		play = true;
		stopAndPlay = false;
		break;
	case OFF:
		play = false;
		stopAndPlay = false;
		break;
	case USE:
		play = true;
		stopAndPlay = true;
		break;
	case SETO1:
		o = (Object*)data;
		break;
	}
}


//=========================================================
// LICZNIKNIK Z PASKEIM 
//=========================================================
VisualTimer::VisualTimer(V2 pos) : Timer()
{
	pb = new Progressbar(pos, V2(0.1f, 0.05f));
}

void VisualTimer::Update(float dt)
{
	Timer::Update(dt);
	pb->value = time;
	pb->max = delay;
	pb->Update();
}

void VisualTimer::Clear()
{
	pb->Remove();
	delete pb;
}

//=========================================================
// CZUJNIK
//=========================================================
Sensor::Sensor(V2 pos) : Object()
{
	type = SENSOR;
	o = NULL;
	message = USE;

	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("package");
	sprite->position = V3(pos.x, pos.y, 0.0f);
	sprite->size = V2(0.125f, 0.125f);
	sprite->uv = V4(0.75f, 0.0, 0.8625f, 0.125f);
	SpriteManager::GetInstance().Add(sprite);

	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixture;

	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x, pos.y);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	
	polygonShape.SetAsBox(0.1f, 0.25f);
	fixture.shape = &polygonShape;
	fixture.density = 1.0f;
	fixture.isSensor = true;
	
	body->CreateFixture(&fixture);
}

void Sensor::Update(float dt)
{
	for( b2ContactEdge * ce = body->GetContactList(); ce; ce = ce->next )
	{
		b2Contact *contact = ce->contact;
		if(contact->IsTouching())
			if(o != NULL) o->Input(this, message, 0, dt);

	}
}

void Sensor::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case SETO1:
		o = (Object*)data;
		break;
	}
}

void Sensor::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
	PhysicsManager::GetInstance().world->DestroyBody(body);
}

//=========================================================
// TASMOCIAG
//=========================================================
Conveyor::Conveyor(V2 pos, UINT size) : Object()
{
	type = CONVEYOR;
	run = false;

	float offset = 0.01f;

	this->size = size;

	sleft = new Sprite();
	sleft->texture = TextureManager::GetInstance().GetTexture("package");
	sleft->uv = V4(0, 0.53125f, 0.0625f, 0.625f);
	sleft->size = V2(0.0625f, 0.1f);
	sleft->position = V3( pos.x - 0.0625f - offset, pos.y, 0.0001f);
	SpriteManager::GetInstance().Add(sleft);

	
	sright = new Sprite();
	sright->texture = TextureManager::GetInstance().GetTexture("package");
	sright->uv = V4(0.0625f, 0.53125f, 0.125f, 0.625f);
	sright->size = V2(0.0625f, 0.1f);
	sright->position = V3( pos.x + ((size-1) * GUI_GRID * 2) + 0.0625f + offset, pos.y, 0.0001f);
	SpriteManager::GetInstance().Add(sright);

	slink = new Sprite*[size];

	for(unsigned int i = 0; i < size; i++)
	{
		slink[i] = new SpriteASAnimation();
		slink[i]->texture = TextureManager::GetInstance().GetTexture("package");
		slink[i]->uv = V4(0.03125f, 0.5f, 0.1142578125f, 0.53125f);
		slink[i]->size = V2(0.125f, 0.0325f);
		slink[i]->position = V3(pos.x + GUI_GRID  * 2 * i, pos.y+0.05f, 0);

		ASANIMATION as;
		as.uv = V4(0.0830078125f, 0, 0.0830078125f, 0);
		as.time = 0;
		((SpriteASAnimation*)(slink[i]))->Add(as);
		
		as.uv = V4(0,0,0,0);
		as.time = 1;
		((SpriteASAnimation*)(slink[i]))->Add(as);

		SpriteManager::GetInstance().Add(slink[i]);

		if(i == 0)
		{
			slink[i]->size = V2(0.115f, 0.0325f);
			slink[i]->position = V3(pos.x + 0.01f , pos.y+0.05f, 0);
		}else if(i == (size - 1))
		{
			slink[i]->size = V2(0.115f, 0.0325f);
			slink[i]->position = V3(pos.x + GUI_GRID * 2 * i - 0.01f , pos.y+0.05f, 0);
		}
	}
	
	slinkrev = new Sprite*[size];

	for(unsigned int i = 0; i < size; i++)
	{
		slinkrev[i] = new SpriteASAnimation();
		slinkrev[i]->texture = TextureManager::GetInstance().GetTexture("package");
		slinkrev[i]->uv = V4(0.03125f, 0.5f, 0.1142578125f, 0.53125f);
		slinkrev[i]->size = V2(0.125f, 0.0325f);
		slinkrev[i]->position = V3(pos.x + GUI_GRID  * 2 * i, pos.y-0.05f, 0);

		ASANIMATION as;
		as.uv = V4(0.0830078125f, 0, 0.0830078125f, 0);
		as.time = 1;
		((SpriteASAnimation*)(slinkrev[i]))->Add(as);
		
		as.uv = V4(0,0,0,0);
		as.time = 0;
		((SpriteASAnimation*)(slinkrev[i]))->Add(as);

		SpriteManager::GetInstance().Add(slinkrev[i]);

		if(i == 0)
		{
			slinkrev[i]->size = V2(0.115f, 0.0325f);
			slinkrev[i]->position = V3(pos.x + 0.01f , pos.y-0.05f, 0);
		}else if(i == (size - 1))
		{
			slinkrev[i]->size = V2(0.115f, 0.0325f);
			slinkrev[i]->position = V3(pos.x + GUI_GRID * 2 * i - 0.01f , pos.y-0.05f, 0);
		}
	}

	//
	// FIZYKA
	//
	
	float s = (size*GUI_GRID);
	const float round = 0.005f;
	const float h = 0.05f;

	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixture;
	
	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x + s - GUI_GRID - 0.01f, pos.y);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	
	s = s - 0.01f;

	b2Vec2 vertices[8];
	int32 count = 8;
	vertices[0].Set(-s + round, h);
	vertices[1].Set(-s, h - round);
	vertices[2].Set(-s, -h + round);
	vertices[3].Set(-s + round, -h);
	vertices[4].Set( s - round, -h);
	vertices[5].Set( s, -h + round);
	vertices[6].Set( s, h - round);
	vertices[7].Set( s - round, h);

	//polygonShape.SetAsBox(s+0.1, 0.05);
	polygonShape.Set(vertices, count);
	fixture.shape = &polygonShape;
	fixture.density = 1.0f;
	fixture.friction = 1.0f;
	
	body->CreateFixture(&fixture);
}

void Conveyor::Update(float dt)
{
	if(run == false)
	{
		for( b2ContactEdge * ce = body->GetContactList(); ce; ce = ce->next )
		{
			b2Contact * c = ce->contact;
			c->GetFixtureA()->GetBody()->SetAwake(true);
			c->GetFixtureB()->GetBody()->SetAwake(true);
			c->SetTangentSpeed(0.0);
		}
	}else{
		for( b2ContactEdge * ce = body->GetContactList(); ce; ce = ce->next )
		{
			b2Contact * c = ce->contact;
			c->GetFixtureA()->GetBody()->SetAwake(true);
			c->GetFixtureB()->GetBody()->SetAwake(true);
			c->SetTangentSpeed(0.25);
		}
	}

	run = false;
}

void Conveyor::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case USE:
		for(int i = 0; i < size; i++)
		{
			((SpriteASAnimation*)slink[i])->Play();
			((SpriteASAnimation*)slinkrev[i])->Play();
		}
		run = true;
		break;
	}
}

void Conveyor::Reset()
{
	for(int i = 0; i < size; i++)
	{
		((SpriteASAnimation*)slink[i])->Pause();
		((SpriteASAnimation*)slinkrev[i])->Pause();
	}
}

void Conveyor::Clear()
{
	SpriteManager::GetInstance().Remove(sleft);
	SpriteManager::GetInstance().Remove(sright);

	for(int i = 0; i < size; i++)
	{
		SpriteManager::GetInstance().Remove(slink[i]);
		SpriteManager::GetInstance().Remove(slinkrev[i]);
	}

	PhysicsManager::GetInstance().world->DestroyBody(body);
}
//=========================================================
// URZEADEZENIE DO MALOWANIA
//=========================================================
Paint::Paint(V2 pos) : Object()
{
	type = PAINTER;

	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("package");
	sprite->position = V3(pos.x, pos.y, 0.0f);
	sprite->size = V2(0.125f, 0.125f);
	sprite->uv = V4(0.625f, 0.0, 0.75f, 0.125f);
	SpriteManager::GetInstance().Add(sprite);

	p = new Particles();
	p->position = V3(pos.x, pos.y-0.20f, 0.01f);
	p->size = V2(0,0);
	p->uv = V4(0.25f, 0.25f, 0.375f, 0.375f);
	p->texture = TextureManager::GetInstance().GetTexture("package");
	p->particleSize = V4(0.05f,0.05f, 0.1f, 0.1f);
	p->zspeed = V2(0.1f,5.0f);
	p->speed = V2(0.2f, 0.3f);
	p->exitAngle = V2(245.0f, 295.0f);
	p->startColor = COLOR(1.0f, 0.3f, 0.3f, 1.0f);
	p->maxPartices = 50;
	p->interval = 0.1f;
	p->StopRespawn();
	SpriteManager::GetInstance().Add(p);

	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixture;

	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x, pos.y-0.25f);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	
	polygonShape.SetAsBox(0.125f, 0.125f);
	fixture.shape = &polygonShape;
	fixture.density = 1.0f;
	fixture.isSensor = true;
	
	body->CreateFixture(&fixture);

	pb = new Progressbar(pos, V2(0.1f,0.05f));
	farba = 100;
	pb->max = farba;
}

void Paint::Update(float dt)
{
	pb->value = farba;
	pb->Update();
}

void Paint::Input(Object *object, MESSAGE type, void* data, float dt)
{
	switch(type)
	{
	case USE:
		{
			if(farba <= 0) break;
			p->PlayRespawn();
			float wyd = 1;
			farba -= wyd * dt;
			for( b2ContactEdge * ce = body->GetContactList(); ce; ce = ce->next )
			{
				b2Contact *contact = ce->contact;
				if(contact->IsTouching())
				{
					if( contact->GetFixtureA()->GetBody() == body )
					{
						Object *tmp = (Object*)(contact->GetFixtureB()->GetBody()->GetUserData());
						tmp->Input(this, PAINT, (void*)&wyd, dt);
					}else{
						Object *tmp = (Object*)(contact->GetFixtureA()->GetBody()->GetUserData());
						tmp->Input(this, PAINT, (void*)&wyd, dt);
					}
				}
			}
		}
		break;

	case LOAD:
		farba +=  100;//*((float*)data);
		break;
	}
}

void Paint::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
	PhysicsManager::GetInstance().world->DestroyBody(body);
	pb->Remove();
	delete pb;
}

//=========================================================
// SKRZYNKA KONCOWA
//=========================================================
FinishBox::FinishBox(V2 pos) : Object()
{
	type = FINISHBOX;

	sk = 0;
	skm = 0;
	sknm = 0;

	sprite = new Sprite();
	sprite->texture = TextureManager::GetInstance().GetTexture("package");
	sprite->position = V3(pos.x, pos.y, 0.001f);
	sprite->size = V2(0.125f,0.125f);
	sprite->uv = V4(0.0, 0.25f, 0.09375f, 0.25f + 0.09375f);
	SpriteManager::GetInstance().Add(sprite);

	b2BodyDef bodyDef;
	b2PolygonShape polygonShape;
	b2FixtureDef fixture;

	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x, pos.y-0.1f);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	
	polygonShape.SetAsBox(0.1f, 0.05f);
	fixture.shape = &polygonShape;
	fixture.density = 1.0f;
	
	body->CreateFixture(&fixture);
}

void FinishBox::Update(float dt)
{
	for( b2ContactEdge * ce = body->GetContactList(); ce; )
	{
		b2Contact* contact = ce->contact;
		if(contact->IsTouching())
		{
			ce = ce->next;
			sk++;
			Object* tmp;
			if(contact->GetFixtureA()->GetBody() == body)
			{
				tmp = (Object*)(contact->GetFixtureB()->GetBody()->GetUserData());
			}else{
				tmp = (Object*)(contact->GetFixtureA()->GetBody()->GetUserData());
			}

			if(tmp->type == OTYPE::PACKAGE_CAN)
			{
				((Can*)tmp)->painting > 0.5 ? skm++ : sknm++;
			}
			ObjectManager::GetInstance().Remove(tmp);
		}else
		{
			ce = ce->next;
		}
	}
}

void FinishBox::Clear()
{
	SpriteManager::GetInstance().Remove(sprite);
	PhysicsManager::GetInstance().world->DestroyBody(body);
}


/*
Package::Package(V2 pos, float mass)
{
	type = PACKAGE;

	sprite = new Sprite();
	sprite->position = V3(pos.x, pos.y, 0);
	sprite->size = V2(0.08, 0.08);
	sprite->texture = TextureManager::GetInstance().GetTexture("pack");
	sprite->uv = V4(0,0.75,0.25,1.0);

	SpriteManager::GetInstance().Add(sprite);
	
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2(pos.x, pos.y);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);

	b2PolygonShape polygonShape;
	polygonShape.SetAsBox(0.03, 0.07);

	body->CreateFixture(&polygonShape, mass);
}

void Package::Update(float dt)
{
	sprite->position = V3(body->GetPosition().x, body->GetPosition().y, 0);
	sprite->angle = Func::RtoD(body->GetAngle());
}

Sensor::Sensor(V2 pos)
{
	sprite = new Sprite();
	sprite->position = V3(pos.x, pos.y, 0);
	sprite->size = V2(0.5, 0.5);
	sprite->texture = TextureManager::GetInstance().GetTexture("pack");
	sprite->uv = V4(0,0.75,0.25,1.0);

	SpriteManager::GetInstance().Add(sprite);
	
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x, pos.y);
	bodyDef.userData = this;
	body = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);

	b2PolygonShape polygonShape;
	b2FixtureDef fd;

	polygonShape.SetAsBox(0.5, 0.5);
	fd.isSensor = true;
	fd.shape = &polygonShape;

	body->CreateFixture(&fd);

	
	p = new Particles();
	p->position = V3(pos.x, pos.y, 0.0f);
	p->size = V2(0,0);
	p->uv = V4(0.25, 0.625, 0.375, 0.75);
	p->texture = TextureManager::GetInstance().GetTexture("pack");
	p->particleSize = V4(0.2,0.2, 0.3, 0.3f);
	p->speed = V2(0.1, 0.3);
	p->exitAngle = V2(0, 360);
	p->maxPartices = 100;
	p->interval = 0.05;
	p->StopRespawn();

	SpriteManager::GetInstance().Add(p);

}

void Sensor::Update(float dt)
{
	sprite->position = V3(body->GetPosition().x, body->GetPosition().y, 0);
	sprite->angle = Func::RtoD(body->GetAngle());

	for( b2ContactEdge * ce = body->GetContactList(); ce; ce = ce->next )
	{
		if( ce->contact != NULL )
		{
			b2Contact * c = ce->contact;
			Object* e = (Object*)c->GetFixtureA()->GetBody()->GetUserData();
			Object* e2 = (Object*)c->GetFixtureB()->GetBody()->GetUserData();
			if(e->type == 1)
			{
				e->Input(this, 0,0);
				p->PlayRespawn();
			
			}
			if(e2->type == 1)
			{
				e2->Input(this, 0,0);
				p->PlayRespawn();
				
			}
		}
	}
}

Conveyor::Conveyor(V2 pos)
{
	itape = 4;
	float size = 2;
	float tw = size/itape/2.0f;
	float th = 0.05;
	float r = 0.15;

	b2BodyDef bodyDef;
	b2CircleShape circleShape;
	b2FixtureDef fd;
	b2PolygonShape polygonShape;

	bodyDef.type = b2_staticBody;
	bodyDef.position = b2Vec2(pos.x, pos.y);
	bodyDef.userData = this;
	circleShape.m_radius = r;
	polygonShape.SetAsBox(tw, th);
	
	roll[0] = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	fd.isSensor = true;
	fd.shape = &circleShape;
	roll[0]->CreateFixture(&fd);

	bodyDef.position = b2Vec2(pos.x + size, pos.y);
	roll[1] = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
	roll[1]->CreateFixture(&fd);

	
	bodyDef.type = b2_dynamicBody;
	for(int i=0; i< itape; i++)
	{
		bodyDef.position = b2Vec2(pos.x+tw+(tw*2*i), pos.y+(r-th));
		tape[i] = PhysicsManager::GetInstance().world->CreateBody(&bodyDef);
		tape[i]->CreateFixture(&polygonShape, 1.0f);
	}

	b2RevoluteJointDef jointDef;
	jointDef.Initialize(roll[0], tape[0], b2Vec2(pos.x, pos.y+r));
	jointDef.enableLimit = false;
	jointDef.lowerAngle = 0;
	jointDef.upperAngle = 0;
	jointDef.enableMotor = false;

	PhysicsManager::GetInstance().world->CreateJoint(&jointDef);

	for(int i=0; i< itape-1; i++)
	{
		jointDef.Initialize(tape[i], tape[i+1], b2Vec2(pos.x + (tw*2*i) + tw*2, pos.y+(r-th)));
		PhysicsManager::GetInstance().world->CreateJoint(&jointDef);
	}

	jointDef.Initialize(tape[itape-1], roll[1], b2Vec2(pos.x+size, pos.y+r));
	PhysicsManager::GetInstance().world->CreateJoint(&jointDef);
	

	sRoll[0] = new Sprite();
	sRoll[0]->texture = TextureManager::GetInstance().GetTexture("pack");
	sRoll[0]->uv = V4(0.25, 0.75, 0.5, 1);
	sRoll[0]->position = V3(pos.x, pos.y, 0);
	sRoll[0]->size = V2(0.15, 0.15);
	
	sRoll[1] = new Sprite();
	sRoll[1]->texture = TextureManager::GetInstance().GetTexture("pack");
	sRoll[1]->uv = V4(0.25, 0.75, 0.5, 1);
	sRoll[1]->position = V3(pos.x+1, pos.y, 0);
	sRoll[1]->size = V2(0.15, 0.15);

	SpriteManager::GetInstance().Add(sRoll[0]);
	SpriteManager::GetInstance().Add(sRoll[1]);
	
	for(int i=0; i< itape; i++)
	{
		sTape[i] = new SpriteASAnimation();
		sTape[i]->texture = TextureManager::GetInstance().GetTexture("pack");
		sTape[i]->uv = V4(0.5, 0.8046875, 0.75, 0.9072265625);
		sTape[i]->position = V3(pos.x+tw+(tw*2*i), pos.y+r, 0);
		sTape[i]->size = V2(tw, th);

		ASANIMATION as;
		as.uv = V4(0.25,0,0.25,0);
		as.time = 0;
		((SpriteASAnimation*)(sTape[i]))->Add(as);
		
		as.uv = V4(0,0,0,0);
		as.time = 0.45;
		((SpriteASAnimation*)(sTape[i]))->Add(as);


		SpriteManager::GetInstance().Add(sTape[i]);
	}
}

void Conveyor::Update(float dt)
{
	for(int i=0; i<2; i++)
	{
		sRoll[i]->position = V3(roll[i]->GetPosition().x, roll[i]->GetPosition().y, 0);
		sRoll[i]->angle = Func::RtoD(roll[i]->GetAngle());
	}

	for(int i=0; i< itape; i++)
	{
		sTape[i]->position = V3(tape[i]->GetPosition().x, tape[i]->GetPosition().y, 0);
		sTape[i]->angle = Func::RtoD(tape[i]->GetAngle());
	}
	
	for(int i=0; i< itape; i++)
	for( b2ContactEdge * ce = tape[i]->GetContactList(); ce; ce = ce->next )
	{
		if( ce->contact != NULL )
		{
			b2Contact * c = ce->contact;
			if( c != NULL )
			{
				c->SetTangentSpeed(1);
			}
		}
	}
}*/

#endif