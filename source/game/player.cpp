#ifndef GAME_PLAYER_CPP
#define GAME_PLAYER_CPP

#include "player.h"

Player& Player::GetInstance()
{
	static Player instance;
	return instance;
}

void Player::Init()
{
	money = 10000;
}

#endif