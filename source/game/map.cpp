#ifndef GAME_MAP_CPP
#define GAME_MAP_CPP

#include "map.h"

Map& Map::GetInstance()
{
	static Map instance;
	return instance;
}

void Map::Init()
{
	sizex = 10;
	sizey = 6;

	map = new Object**[sizex];
	for(unsigned int i = 0; i < sizex; i++)
		map[i] = new Object*[sizey];

	for(unsigned int i = 0; i < sizex; i++)
		for(unsigned int j=0; j < sizey; j++)
			map[i][j] = NULL;
	
	grid = new Sprite();
	grid->texture = TextureManager::GetInstance().GetTexture("grid");
	grid->size = V2(GUI_GRID * sizex, GUI_GRID * sizey);
	grid->uv = V4(0,0, (float)sizex, (float)sizey);
	grid->position = V3(sizex * GUI_GRID, sizey * GUI_GRID, -0.01f);
	SpriteManager::GetInstance().Add(grid);

	CreateMap();
}

void Map::CreateMap()
{
	Sprite* sbg = new Sprite();
	sbg->texture = TextureManager::GetInstance().GetTexture("bg");
	sbg->uv = V4(0,0,4.0f,0.673828125f);
	sbg->position = V3(0, 1.0f,-10.0f);
	sbg->size = V2(8.0f, 2*0.673828125f);
	SpriteManager::GetInstance().Add(sbg);
	
	Sprite* sbg2 = new Sprite();
	sbg2->texture = TextureManager::GetInstance().GetTexture("bg2");
	sbg2->uv = V4(0,0.67382813f,10,1);
	sbg2->position = V3(0,-0.1f,5);
	sbg2->size = V2(10, 1 - 0.67382813f);
	SpriteManager::GetInstance().Add(sbg2);
	
	Sprite* sg = new Sprite();
	sg->texture = TextureManager::GetInstance().GetTexture("bg2");
	sg->uv = V4(0,0,10,0.09375f);
	sg->position = V3(0,-0.15f, 0);
	sg->size = V2(10.0f, 0.15f);
	SpriteManager::GetInstance().Add(sg);

	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f, -1.0f);
	b2Body* groundBody = PhysicsManager::GetInstance().world->CreateBody(&groundBodyDef);
	b2PolygonShape groundBox;
	groundBox.SetAsBox(100.0f, 1.0f);
	groundBody->CreateFixture(&groundBox, 0.0f);
}

bool Map::setObject(unsigned int x, unsigned int y, Object *o)
{
	if( x >= sizex || x < 0 || y >= sizey || y < 0) return false; 
	map[x][y] = o;
	return true;
};

Object* Map::getObject(unsigned int x,unsigned int y)
{
	if( x >= sizex || x < 0 || y >= sizey || y < 0) return NULL; 
	return map[x][y];
};
#endif