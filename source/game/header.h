#ifndef GAME_HEADER_H
#define GAME_HEADER_H

#define BPP 32
#define GUI_GRID 0.125f // 1/2 szerokosci i wyskosoci siatki

#include "../core/core.h"
#include "graphicsobject.h"
#include "obiecttype.h"
#include "objectmanager.h"
#include "objects.h"
#include "player.h"
#include "map.h"
#include "gui.h"

#endif