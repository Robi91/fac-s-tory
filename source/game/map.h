#ifndef GAME_MAP_H
#define GAME_MAP_H

#include "header.h"

class Map
{
private:
	UINT sizex, sizey;
	Object ***map;
	Sprite *grid;

	Map(){};

public:
	static Map& GetInstance();

	void Init();
	Object* getObject(unsigned int x, unsigned int y);
	void CreateMap();
	bool setObject(unsigned int x, unsigned int y, Object *o);
	void HiddenGrid(){};
	void Remove(unsigned int x, unsigned int y)
	{
		if(map[x][y] != NULL)
		{
			ObjectManager::GetInstance().Remove(map[x][y]);
			map[x][y] = NULL;
		}
	}
};

#endif