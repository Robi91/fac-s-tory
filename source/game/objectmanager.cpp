#ifndef GAME_OBJECTMANAGER_CPP
#define GAME_OBJECTMANAGER_CPP

#include "objectmanager.h"

//=========================================================
// OBJECT KOnstruktor
//=========================================================
Object::Object()
{
	type = DEFAULT;
}


//=========================================================
// OBJECTMANAGER singleton
//=========================================================
ObjectManager& ObjectManager::GetInstance()
{
	static ObjectManager instance;
	return instance;
}

//=========================================================
// OBJECTMANAGER Tworzenie
//=========================================================
bool ObjectManager::Init()
{
	return true;
}

//=========================================================
// OBJECTMANAGER Aktualizacja
//=========================================================
void ObjectManager::Update(float dt)
{
	for(unsigned int i = 0; i < objectList.size(); i++)
	{
		objectList[i]->Update(dt);
	}
	/*for(std::vector<Object*>::iterator it = objectList.begin() ; it != objectList.end(); ++it)
		(*it)->Update(dt);*/
}

//=========================================================
// OBJECTMANAGER Reset
//=========================================================
void ObjectManager::Reset()
{
	for(unsigned int i = 0; i < objectList.size(); i++)
	{
		objectList[i]->Reset();
	}
}

//=========================================================
// OBJECTMANAGER Add
//=========================================================
void ObjectManager::Add(Object* object)
{
	objectList.push_back(object);
}

//=========================================================
// OBJECTMANAGER Usuwanie
//=========================================================
void ObjectManager::Remove(Object* object)
{
	for(std::vector<Object*>::iterator it = objectList.begin() ; it != objectList.end(); ++it)
	{
		if((*it) == object)
		{
			(*it)->Clear();
			delete (*it);
			objectList.erase(it);
			return;
		}
	}
}

//=========================================================
// OBJECTMANAGER Usuniecie wszystkich elmentow
//=========================================================
void ObjectManager::RemoveAll()
{
	for(std::vector<Object*>::iterator it = objectList.begin() ; it != objectList.end(); ++it)
	{
		(*it)->Clear();
		delete (*it);
	}

	objectList.clear();
}

#endif