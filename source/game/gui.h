#ifndef GAME_GUI_H
#define GAME_GUI_H

#include "header.h"
#include "player.h"

#define SENSITIVE 0.005f

//=========================================================
// IKONY DO MENU
//=========================================================
struct ICON
{
	enum ID_ICON{
		NONE,
		BACK,
		NEXT,
		REMOVE,
		LOAD,
		REPAIRS,
		HAND,
		POINTER,
		BUILDING,
		CONNECT,
		BUILD_CREATOR,
		BUILD_PAINTER,
		BUILD_SWITCH,
		BUILD_BUTTON,
		BUILD_SENSOR,
		BUILD_CONVEYOR,
		BUILDING2,
		BUILD_FINISH,
		EXIT,
	};

	static V4 uvDark(ID_ICON id)
	{
		V4 ret;
		ret.y = 0.25f;
		ret.w = 0.315f;

		switch(id)
		{
		case ICON::ID_ICON::BUILDING2:
			ret.x = (int)NEXT * 0.0625;
			ret.z = ret.x + 0.0625f;
			break;
		case ICON::ID_ICON::BUILD_FINISH:
			ret.y += 0.0625*2;
			ret.w += 0.0625*2;
			ret.x = 0;
			ret.z = ret.x + 0.0625f;
			break;
		case ICON::ID_ICON::EXIT:
			ret.y += 0.0625*2;
			ret.w += 0.0625*2;
			ret.x = 0.0625f;
			ret.z = ret.x + 0.0625f;
			break;
		default:
			ret.x = (int)id * 0.0625f;
			ret.z = ret.x + 0.0625f;
		}

		return ret;
	}

	static V4 uvLight(ID_ICON id)
	{
		V4 r = ICON::uvDark(id);
		r.y += 0.0625;
		r.w += 0.0625;
		return r;
	}

	static std::string getText(ID_ICON id)
	{
		std::string ret = "---";
		switch(id)
		{
		case 1:
			ret = "Powrot";
			break;
		case 8:
			ret = Language::GetInstance().GetString(0);
			break;
		}
		return ret;
	}
};

//=========================================================
// KURSORY
//=========================================================
struct CURSOR
{
	enum ID_CURSOR{
		POINTER,
		HAND,
		CONNECT,
		BUILD,
		BUILD_BUTTON,
		BUILD_SWITCH,
		BUILD_CAN_CREATOR,
		BUILD_PAINTER,
		BUILD_CONVEYOR3,
		BUILD_CONVEYOR5,
		BUILD_SENSOR,
		BUILD_FINISH,
	} active;
	Sprite *cursor;
	Object* selectObject;
	Sprite* select;

	CURSOR()
	{
		active = POINTER;
		selectObject = NULL;
		cursor = new Sprite();
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.08f, 0.08f);
		cursor->uv = V4(0,0,0.09375, 0.09375);

		SpriteManager::GetInstance().Add(cursor);
	}

	void Change(ID_CURSOR id)
	{
		switch(id)
		{
		case HAND:
			cursor->uv = V4(0.09375, 0, 0.1875, 0.09375);
			cursor->size = V2(0.1f, 0.1f);
			break;

		case CONNECT:
			cursor->uv = V4(0, 0.09375, 0.09375, 0.1875);
			cursor->size = V2(0.1f, 0.1f);
			break;

		default:
			cursor->uv = V4(0,0,0.09375, 0.09375);
			cursor->size = V2(0.08f, 0.08f);
		}

		active = id;
	}

};

//=========================================================
// MENU PODRECZNE
//=========================================================
#define MENU_R 0.3f
#define MENU_SIZE 0.15f
#define MENU_ICON 0.1f
#define MENU_Z 1.0f
#define MENU_Z_SELECT 5.0f

class Menu
{
public:
	V2 pos;
	ICON::ID_ICON icon[8];
	int select;
	Sprite* bg[8];
	Sprite* icons[8];
	Text* text;

	void Init()
	{

		select = -1;
		pos = V2(1,1);
		
		for(int i = 0; i < 8; i++)
		{
			icon[i] = ICON::ID_ICON::REMOVE;
		}

		text = new Text();
		text->texture = TextureManager::GetInstance().GetTexture("font");
		text->hidden = true;
		text->size.y = 0.08f;
		SpriteManager::GetInstance().Add(text);

		for(int i = 0; i < 8; i++)
		{
			float a = Func::DtoR(i*45.0f);
			bg[i] = new Sprite();
			bg[i]->texture = TextureManager::GetInstance().GetTexture("gui");
			bg[i]->uv = V4(0.5, 0.0, 0.59375, 0.09375);
			bg[i]->angle = -i*45.0f;
			float x = sin(a) * MENU_R + pos.x;
			float y = cos(a) * MENU_R + pos.y;
			bg[i]->position = V3(x, y, MENU_Z);
			bg[i]->size = V2(MENU_SIZE, MENU_SIZE);
			bg[i]->color = COLOR(0,0,0,0.9f);
			bg[i]->hidden = true;
			
			SpriteManager::GetInstance().Add(bg[i]);
	
			icons[i] = new Sprite();
			icons[i]->texture = TextureManager::GetInstance().GetTexture("gui");
			icons[i]->uv = ICON::uvLight(icon[i]);
			icons[i]->position = V3( x, y, MENU_Z + 0.1f);
			icons[i]->size = V2(MENU_ICON, MENU_ICON);
			icons[i]->color = COLOR::SetOne();
			icons[i]->hidden = true;

			SpriteManager::GetInstance().Add(icons[i]);
		}
	}

	void Show(V2 pos)
	{

		text->hidden = false;
		text->text = "Menu";
		text->position.y = pos.y;
		text->position.x = pos.x + MENU_Z / 2;

		this->pos = pos;
		for(int i = 0; i < 8; i++)
		{
			float a = Func::DtoR(i*45.0f);
			bg[i]->angle = -i*45.0f;
			float x = sin(a) * MENU_R + pos.x;
			float y = cos(a) * MENU_R + pos.y;
			bg[i]->position = V3(x, y, MENU_Z);
			bg[i]->color = COLOR(0,0,0,0.9f);
			bg[i]->hidden = false;
		
			icons[i]->position = V3( x, y, MENU_Z + 0.1f);
			icons[i]->uv = ICON::uvLight(icon[i]);
			icons[i]->size = V2(MENU_ICON, MENU_ICON);
			icons[i]->hidden = false;
		}

	}

	void Hide()
	{
		text->hidden = true;
		for(int i = 0; i < 8; i++)
		{
			bg[i]->hidden = true;
			icons[i]->hidden = true;
		}
	}

	void Select(V2 vect)
	{
		float ang = Func::RtoD(atan2(vect.x,vect.y));
		float z = sqrt(pow(vect.x,2)+pow(vect.y,2));

		if(z < 15) return;

		if(ang > 160 || ang < -160)
		{
			SelectButton(0);
		}else if(ang > -157.5 && ang < -117.5)
		{
			SelectButton(7);
		}else if(ang > -115 && ang < -75)
		{
			SelectButton(6);
		}else if(ang > -72.5 && ang < -32.5)
		{
			SelectButton(5);
		}else if(ang > -30 && ang < 10)
		{
			SelectButton(4);
		}else if(ang > 12.5 && ang < 52.5)
		{
			SelectButton(3);
		}else if(ang > 55 && ang < 95)
		{
			SelectButton(2);
		}else if(ang > 97.5 && ang < 137.5)
		{
			SelectButton(1);
		}
	}

	void SelectButton(int id)
	{
		if(select != -1)
		{
			icons[select]->uv = ICON::uvLight(icon[select]);
			bg[select]->color = COLOR(0,0,0,0.9f);
			icons[select]->position.z = MENU_Z + 0.1f;
			bg[select]->position.z = MENU_Z;
		}
		select = id;
		text->text = ICON::getText(icon[select]);
		icons[select]->uv = ICON::uvDark(icon[select]);
		bg[select]->color = COLOR(1,1,1,1);
		icons[select]->position.z = MENU_Z_SELECT + 0.1f;
		bg[select]->position.z = MENU_Z_SELECT;
	}

	void SetIcons(ICON::ID_ICON ic[8])
	{
		for(int i = 0; i < 8; i++)
		{
			icon[i] = ic[i];
		}
	}
};

//=========================================================
// GUI
//=========================================================
enum MODE{
	NORMAL,
	MENU
};

class GUI
{
protected:
	MODE mode;
	V4 field;
	CURSOR mouse;
	Menu menu;
	Text *info;
	int WIDTH, HEIGHT;

public:
	static GUI& GetInstance();

	void Init();
	void Input(float dt);
	void Update(float dt);
	void MenuMode(float dt);
	void NormalMode(float dt);
};
#endif