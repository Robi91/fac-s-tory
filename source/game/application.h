#ifndef GAME_APPLICATION_H
#define GAME_APPLICATION_H

#include "header.h"

//=========================================================
// STATE
//=========================================================
class State
{
public:	
	bool quit;
	virtual void Init() = 0;
	virtual void UpdateInput(float dt) = 0;
	virtual void UpdatePhysics(float dt) = 0;
	virtual void UpdateGraphics(float dt) = 0;
};

//=========================================================
// GAMEPLAY
//=========================================================
class Gameplay : public State
{
public:	
	CanCreator* cc;
	Can* c;
	Paint *p;
	FinishBox* fb;
	VisualTimer* t;
	Button* butt;
	Switch* butt2;

	void Init();
	void UpdateInput(float dt);
	void UpdatePhysics(float dt);
	void UpdateGraphics(float dt);
};

//=========================================================
// APPLICATION
//=========================================================
class Application
{
public:
	
	Application();

	void Run();

private:
	State *game, *menu;
	int state;

	bool Init();
};

#endif