#ifndef GAME_GRAPHICSOBJECTS_H
#define GAME_GRAPHICSOBJECTS_H

#include "header.h"

class Progressbar
{
public:
	Sprite *s1;
	Sprite *s2;
	float min;
	float max;
	float value;

	Progressbar(V2 pos, V2 size)
	{
		min = 0.0f;
		max = 1.0f;
		value = 0.0f;

		s1 = new Sprite();
		s1->texture = TextureManager::GetInstance().GetTexture("package");
		s1->position = V3(pos.x, pos.y, 0.05f);
		s1->size = size;
		s1->uv = V4(0.0625, 0.125, 0.1875, 0.1875);
		SpriteManager::GetInstance().Add(s1);
	
		s2 = new Sprite();
		s2->texture = TextureManager::GetInstance().GetTexture("package");
		s2->position = V3(pos.x, pos.y, 0.051f);
		s2->size = size;
		s2->uv = V4(0.0625, 0.1875, 0.0625, 0.25);
		SpriteManager::GetInstance().Add(s2);
	}

	void Update()
	{
		float percent = value / (max - min);
		if(percent > 1.0f) percent = 1.0f;
		if(percent < 0.0f) percent = 0.0f;
		float x = s1->position.x - (1-percent) * s1->size.x;
		float w = percent * s1->size.x;
		float u = s1->uv.x + ( percent * ( s1->uv.z - s1->uv.x ) );

		s2->position.x = x;
		s2->size.x = w;
		s2->uv.z = u;
	}

	void Remove()
	{
		SpriteManager::GetInstance().Remove(s1);
		SpriteManager::GetInstance().Remove(s2);
	}
};

#endif