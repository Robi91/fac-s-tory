#ifndef GAME_OBJECTMANAGER_H
#define GAME_OBJECTMANAGER_H

#include "../core/core.h"
#include "obiecttype.h"

class Object
{
public:
	OTYPE type;
	
	Object();
	virtual void Clear() = 0;
	virtual void Reset() = 0;
	virtual void Update(float dt) = 0;
	virtual void Input(Object *object, MESSAGE type, void* data, float dt) = 0;
};

class ObjectManager
{
public:
	static ObjectManager& GetInstance();

	bool Init();
	void Update(float dt);
	void Reset();
	void Add(Object* object);
	void Remove(Object* object);
	void RemoveAll();
	
private:
	std::vector<Object*> objectList;

	ObjectManager() {};
	~ObjectManager() {};
};

#endif