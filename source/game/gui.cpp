#ifndef GAME_GUI_CPP
#define GAME_GUI_CPP

#include "gui.h"

GUI& GUI::GetInstance()
{
	static GUI instance;
	return instance;
}

void GUI::Init()
{
	WIDTH = Settings::GetInstance().GetInt("width");
	HEIGHT = Settings::GetInstance().GetInt("height");

	mode = NORMAL;
	field = V4(-0.5f,5.0f,0.0f,3.0f);
	Camera::GetInstance().screen = field;

	info = new Text();
	info->texture = TextureManager::GetInstance().GetTexture("font");
	info->size.y = 0.06f;
	info->position = V3(-0.9f,0.5f,0);
	SpriteManager::GetInstance().Add2D(info);

	SDL_ShowCursor(0);
	SDL_WM_GrabInput(SDL_GRAB_ON);
	SDL_WarpMouse(WIDTH/2, HEIGHT/2);
	
	Camera::GetInstance().SetRequiredPosition(V3(0,CORE_CAMERA_RATIO,0));

	menu.Init();
}

void GUI::Input(float dt)
{
	switch(mode)
	{
	case NORMAL:
		NormalMode(dt);
		break;
	case MENU:
		MenuMode(dt);
		break;
	}
	
	if(Input::GetInstance().MouseTapped(SDL_BUTTON_RIGHT))
	{
		if(mode == NORMAL)
		{
			mode = MENU;
			ICON::ID_ICON m[] = { ICON::ID_ICON::HAND, ICON::ID_ICON::CONNECT, ICON::ID_ICON::BUILDING, ICON::ID_ICON::NONE, ICON::ID_ICON::EXIT, ICON::ID_ICON::NONE, ICON::ID_ICON::BACK, ICON::ID_ICON::POINTER };   
			menu.SetIcons(m);
			menu.Show(V2(mouse.cursor->position.x, mouse.cursor->position.y));
			mouse.cursor->hidden = true;
		}else{
			menu.Hide();
			mode = NORMAL;
			mouse.cursor->hidden = false;
		}
	}
	
	SDL_WarpMouse(WIDTH/2, HEIGHT/2);
}

void GUI::Update(float dt)
{
	char t[14];
	sprintf(t, "%i$", Player::GetInstance().money);
	info->text = t;
}

void GUI::MenuMode(float dt)
{
	V2 pos = Input::GetInstance().GetMousePosition();
	
	int x,y;

	x = (int)(pos.x - WIDTH/2);
	y = (int)(pos.y - HEIGHT/2);

	menu.Select(V2((float) x, (float)y));

	
	x = (int)floor(mouse.cursor->position.x/(GUI_GRID*2));
	y = (int)floor(mouse.cursor->position.y/(GUI_GRID*2));

	if(Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT))
	{
		switch(menu.icon[menu.select])
		{
		case ICON::ID_ICON::EXIT:
			SDL_Event e;
			e.type = SDL_QUIT;
			SDL_PushEvent(&e);
			break;
		case ICON::ID_ICON::POINTER:
			mouse.Change(CURSOR::POINTER);
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			break;

		case ICON::ID_ICON::HAND:
			mouse.Change(CURSOR::HAND);
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			break;

		case ICON::ID_ICON::CONNECT:
			mouse.Change(CURSOR::CONNECT);
			menu.Hide();
			mouse.cursor->hidden = false;
			/*
			mouse.selectObject = Map::GetInstance().getObject(x,y);
			mouse.select = new Sprite();
			mouse.select->texture = TextureManager::GetInstance().GetTexture("gui");
			mouse.select->position = V3(x*GUI_GRID*2 + GUI_GRID, y*GUI_GRID*2 + GUI_GRID, 1.0f);
			mouse.select->size = V2(0.16, 0.16);
			mouse.select->uv = V4(0.5, 0.09375, 0.65625, 0.25);
			SpriteManager::GetInstance().Add(mouse.select);
			*/
			mode = MODE::NORMAL;
			break;

		case ICON::ID_ICON::REMOVE:
			Map::GetInstance().Remove(x,y);
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			break;

		case ICON::ID_ICON::BUILDING:
			{
				menu.Hide();
				ICON::ID_ICON m[] = { ICON::ID_ICON::BUILD_SWITCH, ICON::ID_ICON::BUILD_SENSOR, ICON::ID_ICON::BUILD_CREATOR, ICON::ID_ICON::BUILD_PAINTER, ICON::ID_ICON::BUILD_CONVEYOR, ICON::ID_ICON::BUILDING2, ICON::ID_ICON::BACK, ICON::ID_ICON::BUILD_BUTTON };   
				menu.SetIcons(m);
				menu.Show(menu.pos);
			}
			break;

		case ICON::ID_ICON::BUILDING2:
			{
				menu.Hide();
				ICON::ID_ICON m[] = { ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::BACK, ICON::ID_ICON::BUILD_FINISH };   
				menu.SetIcons(m);
				menu.Show(menu.pos);
			}
			break;

		case ICON::ID_ICON::BACK:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			break;

		case ICON::ID_ICON::BUILD_BUTTON:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_BUTTON);
			break;

		case ICON::ID_ICON::BUILD_SWITCH:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_SWITCH);
			break;

		case ICON::ID_ICON::BUILD_CREATOR:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_CAN_CREATOR);
			break;

		case ICON::ID_ICON::BUILD_PAINTER:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_PAINTER);
			break;

		case ICON::ID_ICON::BUILD_CONVEYOR:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_CONVEYOR3);
			break;

		case ICON::ID_ICON::BUILD_FINISH:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_FINISH);
			break;

		case ICON::ID_ICON::BUILD_SENSOR:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			mouse.Change(CURSOR::BUILD_SENSOR);
			break;
			
		case ICON::ID_ICON::LOAD:
			menu.Hide();
			mouse.cursor->hidden = false;
			mode = MODE::NORMAL;
			Map::GetInstance().getObject(x,y)->Input(0, LOAD, 0, dt);
			break;
		}
	}
}

void GUI::NormalMode(float dt)
{
	V2 pos = Input::GetInstance().GetMousePosition();
	if(!(pos.x == WIDTH/2 && pos.y == HEIGHT/2))
	{
		int x,y;

		x = (int)(pos.x - WIDTH/2);
		y = (int)(pos.y - HEIGHT/2);

		mouse.cursor->position.x = mouse.cursor->position.x + (x) * SENSITIVE;
		mouse.cursor->position.y = mouse.cursor->position.y + (-y) * SENSITIVE;
		mouse.cursor->position.z = 0.1f;

		if(mouse.cursor->position.x < field.x) mouse.cursor->position.x = field.x;
		if(mouse.cursor->position.x > field.y) mouse.cursor->position.x = field.y;
		if(mouse.cursor->position.y < field.z) mouse.cursor->position.y = field.z;
		if(mouse.cursor->position.y > field.w) mouse.cursor->position.y = field.w;

		Camera::GetInstance().SetRequiredPosition(mouse.cursor->position);
	}

	if(Input::GetInstance().MousePressed(SDL_BUTTON_LEFT))
	{
		int x = (int)floor(mouse.cursor->position.x/(GUI_GRID*2));
		int y = (int)floor(mouse.cursor->position.y/(GUI_GRID*2));
		
		float mx = x * GUI_GRID*2 + GUI_GRID;
		float my = y * GUI_GRID*2 + GUI_GRID;

		Object *o = Map::GetInstance().getObject(x,y);

		switch(mouse.active)
		{
			case CURSOR::ID_CURSOR::HAND:
				if(o == NULL) break;
				switch(o->type)
				{
				case BUTTON:
					o->Input(0, USE, 0, dt);
					break;

				default:
					if(Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT))
						o->Input(0, USE, 0, dt);
				}
			break;

			case CURSOR::ID_CURSOR::POINTER:
				{
				if(o == NULL) break;
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				ICON::ID_ICON m[] = { ICON::ID_ICON::CONNECT, ICON::ID_ICON::NONE, ICON::ID_ICON::NONE,
								      ICON::ID_ICON::NONE, ICON::ID_ICON::NONE, ICON::ID_ICON::REMOVE, 
									  ICON::ID_ICON::BACK, ICON::ID_ICON::NONE };   
				switch(o->type)
				{
				case BUTTON:
					//o->Input(0, USE, 0, dt);
					break;

				case PAINTER:
					m[0] = ICON::ID_ICON::LOAD;
					m[5] = ICON::ID_ICON::REMOVE;
					//o->Input(0, LOAD, 0, dt);
					break;

				default:
					m[5] = ICON::ID_ICON::REMOVE;
				}

				menu.SetIcons(m);
				menu.Show(V2(mx, my));
				mouse.selectObject = o;
				mode = MENU;
				}
			break;
			
			case CURSOR::ID_CURSOR::CONNECT:
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o == NULL) break;
				if(mouse.selectObject == NULL)
				{
					mouse.selectObject = o;
					mouse.select = new Sprite();
					mouse.select->texture = TextureManager::GetInstance().GetTexture("gui");
					mouse.select->position = V3(mx, my, 1.0f);
					mouse.select->size = V2(0.16f, 0.16f);
					mouse.select->uv = V4(0.5f, 0.09375f, 0.65625f, 0.25f);
					SpriteManager::GetInstance().Add(mouse.select);
				}else{
					mouse.selectObject->Input(0,SETO1, (void*)o, dt);
					mouse.selectObject = NULL;
					
					SpriteManager::GetInstance().Remove(mouse.select);
				}
			break;

			case CURSOR::ID_CURSOR::BUILD_BUTTON:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Button(V2(mx,my));

				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
				}else{
					no->Clear();
					delete no;
				}

				}
				break;

			case CURSOR::ID_CURSOR::BUILD_SWITCH:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Switch(V2(mx,my));
				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
				}else{
					no->Clear();
					delete no;
				}
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_CAN_CREATOR:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new CanCreator(V2(mx,my));
				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
					Player::GetInstance().money -= 1000;
				}else{
					no->Clear();
					delete no;
				}
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_PAINTER:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Paint(V2(mx,my));

				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
				}else{
					no->Clear();
					delete no;
				}
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_CONVEYOR3:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Conveyor(V2(mx,my), 3);
				Map::GetInstance().setObject(x,y,no);
				Map::GetInstance().setObject(x + 1,y,no);
				Map::GetInstance().setObject(x + 2,y,no);
				ObjectManager::GetInstance().Add(no);
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_CONVEYOR5:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Conveyor(V2(mx,my), 5);
				Map::GetInstance().setObject(x,y,no);
				Map::GetInstance().setObject(x + 1,y,no);
				Map::GetInstance().setObject(x + 2,y,no);
				Map::GetInstance().setObject(x + 3,y,no);
				Map::GetInstance().setObject(x + 4,y,no);
				ObjectManager::GetInstance().Add(no);
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_SENSOR:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new Sensor(V2(mx,my));

				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
				}else{
					no->Clear();
					delete no;
				}
				}
				break;

			case CURSOR::ID_CURSOR::BUILD_FINISH:
				{
				if(!Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT)) break;
				if(o != NULL) break;
				Object *no = new FinishBox(V2(mx,my));

				if(Map::GetInstance().setObject(x,y,no))
				{
					ObjectManager::GetInstance().Add(no);
				}else{
					no->Clear();
					delete no;
				}
				}
				break;
		}
	}
}
#endif


/*
SMIECI



	if(Input::GetInstance().MousePressed(SDL_BUTTON_LEFT))
	{
		int x = floor(cursor->position.x/(GUI_GRID*2));
		int y = floor(cursor->position.y/(GUI_GRID*2));

		int xm = x + (gridx / 2);
		int ym = y;

		bool tapped = Input::GetInstance().MouseTapped(SDL_BUTTON_LEFT);

		switch(active)
		{
		case DEFAULT:
				if(map[xm][ym] != NULL)
				{
					switch(map[xm][ym]->type)
					{
					case BUTTON:
						map[xm][ym]->Input(0,USE,0,0);
						break;

					default:
						if(!tapped) break;
						map[xm][ym]->Input(0,USE,0,0);
						break;
					}
				}
			break;
		case LINK:
				if(!tapped) break;
				if(map[xm][ym] != NULL)
				{
					if(selectObject == NULL)
					{
						selectObject = map[xm][ym];
					}else{
						selectObject->Input(0, SETO1, (void*)map[xm][ym], 0);
						selectObject = NULL;
					}
				}
			break;
		case REMOVE:
				if(!tapped) break;
				if(map[xm][ym] != NULL)
				{
					ObjectManager::GetInstance().Remove(map[xm][ym]);
					map[xm][ym] = NULL;
				}
			break;
		case PACKAGECREATOR_CAN:
			{
				if(!tapped) break;
				if(map[xm][ym] != NULL) break;

				Object* tmp = new CanCreator(V2(x*(GUI_GRID*2)+GUI_GRID,y*(GUI_GRID*2)+GUI_GRID));
				ObjectManager::GetInstance().Add(tmp);
				map[xm][ym] = tmp;
		
			}
			break;

		case FINISHBOX:
			{
				if(!tapped) break;
				if(map[xm][ym] != NULL) break;
				Object* tmp = new FinishBox(V2(x*(GUI_GRID*2)+GUI_GRID,y*(GUI_GRID*2)+GUI_GRID));
				ObjectManager::GetInstance().Add(tmp);
				map[xm][ym] = tmp;
			}
			break;
		case BUTTON:
			{
				if(!tapped) break;
				if(map[xm][ym] != NULL) break;
				Object* tmp = new Button(V2(x*(GUI_GRID*2)+GUI_GRID,y*(GUI_GRID*2)+GUI_GRID));
				ObjectManager::GetInstance().Add(tmp);
				map[xm][ym] = tmp;
			}
			break;
		case SWITCH:
			{
				if(!tapped) break;
				if(map[xm][ym] != NULL) break;
				Object* tmp = new Switch(V2(x*(GUI_GRID*2)+GUI_GRID,y*(GUI_GRID*2)+GUI_GRID));
				ObjectManager::GetInstance().Add(tmp);
				map[xm][ym] = tmp;
			}
			break;
		case TIMER:
			{
				if(!tapped) break;
				if(map[xm][ym] != NULL) break;
				Object* tmp = new VisualTimer(V2(x*(GUI_GRID*2)+GUI_GRID,y*(GUI_GRID*2)+GUI_GRID));
				ObjectManager::GetInstance().Add(tmp);
				map[xm][ym] = tmp;
			}
			break;
		}
	}




if(Input::GetInstance().KeyTapped(SDLK_0))
	{
		active = OTYPE::LINK;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.05, 0.05);
		cursor->uv = V4(0.0625,0,0.125,0.0625);
		cursor->color.z = 1;
		selectObject = NULL;
	}
	if(Input::GetInstance().KeyTapped(SDLK_1))
	{
		active = OTYPE::DEFAULT;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.05, 0.05);
		cursor->uv = V4(0,0,0.0625, 0.0625);
		cursor->color.z = 1;
	}
	if(Input::GetInstance().KeyTapped(SDLK_2))
	{
		active = OTYPE::PACKAGECREATOR_CAN;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.05, 0.05);
		cursor->uv = V4(0,0,0.0625, 0.0625);
	}
	if(Input::GetInstance().KeyTapped(SDLK_3))
	{
		active = OTYPE::FINISHBOX;
		cursor->texture = TextureManager::GetInstance().GetTexture("package");
		cursor->size = V2(0.125,0.125);
		cursor->uv = V4(0.0, 0.25, 0.09375, 0.25 + 0.09375);
		cursor->color.w = 0.5;
	}
	if(Input::GetInstance().KeyTapped(SDLK_4))
	{
		active = OTYPE::SWITCH;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.08,0.08);
		cursor->uv = V4(0.34375, 0.0, 0.4365234375, 0.09375);
		cursor->color.w = 0.5;
	}
	if(Input::GetInstance().KeyTapped(SDLK_5))
	{
		active = OTYPE::BUTTON;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.08,0.08);
		cursor->uv = V4(0.25, 0.0, 0.34375, 0.09375);
		cursor->color.w = 0.5;
	}
	if(Input::GetInstance().KeyTapped(SDLK_6))
	{
		active = OTYPE::TIMER;
		cursor->texture = TextureManager::GetInstance().GetTexture("package");
		cursor->size = V2(0.125,0.125);
		cursor->uv = V4(0.0, 0.25, 0.09375, 0.25 + 0.09375);
		cursor->color.w = 0.5;
	}
	if(Input::GetInstance().KeyTapped(SDLK_9))
	{
		active = OTYPE::REMOVE;
		cursor->texture = TextureManager::GetInstance().GetTexture("gui");
		cursor->size = V2(0.08,0.08);
		cursor->uv = V4(0.0, 0.0625, 0.0625, 0.125);
		cursor->color.w = 0.5;
	}

*/