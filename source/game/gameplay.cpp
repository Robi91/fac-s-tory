#ifndef GAME_GAMEPLAY_CPP
#define GAME_GAMEPLAY_CPP

#include "application.h"

//=========================================================
// GAMEPLAY Ladownie danych
//=========================================================
void Gameplay::Init()
{
	TextureManager::GetInstance().LoadTexture("media/texture/package.png", "package");
	TextureManager::GetInstance().LoadTexture("media/texture/bg.png", "bg");
	TextureManager::GetInstance().LoadTexture("media/texture/bg2.png", "bg2", GL_NEAREST);
	TextureManager::GetInstance().LoadTexture("media/texture/gui.png", "gui");
	TextureManager::GetInstance().LoadTexture("media/texture/grid.png", "grid", GL_NEAREST);
	TextureManager::GetInstance().LoadTexture("media/font/font.png", "font");

	Player::GetInstance().Init();
	Map::GetInstance().Init();
	GUI::GetInstance().Init();
	quit = false;
}

//=========================================================
// GAMEPLAY Aktualizacja danych - przyciski itp
//=========================================================
void Gameplay::UpdateInput(float dt) 
{
	if(Input::GetInstance().KeyTapped(SDLK_ESCAPE))
	{
		quit = true;
	}

	GUI::GetInstance().Input(dt);
}

//=========================================================
// GAMEPLAY Aktualizacja fizyki
//=========================================================
void Gameplay::UpdatePhysics(float dt)
{
}

//=========================================================
// GAMEPLAY Aktualizacja grafiki
//=========================================================
void Gameplay::UpdateGraphics(float dt)
{
	GUI::GetInstance().Update(dt);
}

#endif


/*

SMIECI

if(Input::GetInstance().KeyPressed(SDLK_PAGEUP))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.z += 1;
		Camera::GetInstance().SetRequiredPosition(p);
	}
	if(Input::GetInstance().KeyPressed(SDLK_PAGEDOWN))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.z -= 1;
		Camera::GetInstance().SetRequiredPosition(p);
	}
	if(Input::GetInstance().KeyPressed(SDLK_LEFT))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.x -= 0.1;
		Camera::GetInstance().SetRequiredPosition(p);
	}
	if(Input::GetInstance().KeyPressed(SDLK_RIGHT))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.x += 0.1;
		Camera::GetInstance().SetRequiredPosition(p);
	}
	if(Input::GetInstance().KeyPressed(SDLK_UP))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.y += 0.1;
		Camera::GetInstance().SetRequiredPosition(p);
	}
	if(Input::GetInstance().KeyPressed(SDLK_DOWN))
	{
		V3 p = Camera::GetInstance().GetRequiredPosition();
		p.y -= 0.1;
		Camera::GetInstance().SetRequiredPosition(p);
	}

	if(Input::GetInstance().KeyTapped(SDLK_z))
	{
		c = new Can(V2(-0.5, 0));
		ObjectManager::GetInstance().Add(c);
	}
	if(Input::GetInstance().KeyTapped(SDLK_x))
	{
		c = new Can(V2(0.5, 0));
		ObjectManager::GetInstance().Add(c);
	}

	if(Input::GetInstance().KeyTapped(SDLK_c))
	{
		ObjectManager::GetInstance().Remove(c);
	}
	if(Input::GetInstance().KeyPressed(SDLK_v))
	{
		p->Input(0, USE, 0, dt);
	}
	if(Input::GetInstance().KeyPressed(SDLK_b))
	{
		t->Input(0,ON, 0,dt);
	}
	if(Input::GetInstance().KeyPressed(SDLK_n))
	{
		t->Input(0,USE, 0,dt);
	}
	if(Input::GetInstance().KeyTapped(SDLK_h))
	{
		GUI::GetInstance().HiddeGrid();
	}

	if(Input::GetInstance().KeyPressed(SDLK_l))
	{
		butt->Input(0,USE,0,0);
	}
	if(Input::GetInstance().KeyTapped(SDLK_o))
	{
		butt2->Input(0,USE,0,0);
	}

*/