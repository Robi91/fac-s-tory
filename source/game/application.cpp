#ifndef GAME_APPLICATION_CPP
#define GAME_APPLICATION_CPP

#include "application.h"


//=========================================================
// APPLICATION KONSTURKOT
//=========================================================
Application::Application()
{
	state = 1;
}

//=========================================================
// APPLICATION G��wna p�tla aplikacji
//=========================================================
void Application::Run()
{
	bool quit = false;
	float dt = 0.0f;
	UINT prevTime;

	if(!Init()) quit = true;

	prevTime = SDL_GetTicks();

	while(!quit)
	{
		UINT time = SDL_GetTicks();
		UINT tt = (time - prevTime);
		dt = (time - prevTime) / 1000.0f;
		prevTime = time;
		if(dt > 0.025f) dt = 0.025f;
		
		switch(state)
		{
		case 0:
			if(!Input::GetInstance().Update()) quit = true;

			PhysicsManager::GetInstance().world->Step(dt, 6, 2);
			ObjectManager::GetInstance().Update(dt);

			Camera::GetInstance().Update(dt);
			SpriteManager::GetInstance().Update(dt);
		
			SpriteManager::GetInstance().Render();
			Graphics::GetInstance().Flush();
			break;

		case 1:
			ObjectManager::GetInstance().Reset();

			if(!Input::GetInstance().Update() || game->quit == true) quit = true;
			game->UpdateInput(dt);
			
			PhysicsManager::GetInstance().world->Step(dt, 6, 2);
			ObjectManager::GetInstance().Update(dt);
			game->UpdatePhysics(dt);
			
			Camera::GetInstance().Set3D();
			Camera::GetInstance().Update(dt);
			SpriteManager::GetInstance().Update(dt);
			game->UpdateGraphics(dt);
		
			SpriteManager::GetInstance().Render();
			Camera::GetInstance().Set2D();
			SpriteManager::GetInstance().Render2D();
			Graphics::GetInstance().Flush();
			break;
		}
		char e[64];
		sprintf(e,"DT: %f", dt);
		SDL_WM_SetCaption(e, NULL);
	}
}

//=========================================================
// APPLICATION Init
//=========================================================
bool Application::Init()
{
	Language::GetInstance().Init("media/lang/en.lng");
	Settings::GetInstance().Init("media/settings.fs");

	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) return false;    

	if (Settings::GetInstance().GetBool("fullscreen"))
	{
		if (SDL_SetVideoMode(Settings::GetInstance().GetInt("width"), Settings::GetInstance().GetInt("height"), BPP, SDL_OPENGL | SDL_FULLSCREEN) == NULL) return false;
	}
	else{
		if (SDL_SetVideoMode(Settings::GetInstance().GetInt("width"), Settings::GetInstance().GetInt("height"), BPP, SDL_OPENGL) == NULL) return false;
	}
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	Input::GetInstance().Init();
	TextureManager::GetInstance().Init();
	Graphics::GetInstance().Init();
	PhysicsManager::GetInstance().world = new b2World(b2Vec2(0.0f, -10.0f));
	Camera::GetInstance().Init();
	Player::GetInstance().Init();
	SpriteManager::GetInstance().Init();
	ObjectManager::GetInstance().Init();


	game = new Gameplay();
	game->Init();

	return true;
}
#endif