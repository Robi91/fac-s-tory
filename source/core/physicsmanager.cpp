#ifndef CORE_PHYSICSMANAGER_CPP
#define CORE_PHYSICSMAANGER_CPP

#include "physicsmanager.h"

//=========================================================
// PHYSICSMANAGER SINGLETON
//=========================================================
PhysicsManager& PhysicsManager::GetInstance()
{
	static PhysicsManager instance;
	return instance;
}

#endif