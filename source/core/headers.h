#ifndef CORE_HEADERS_H
#define CORE_HEADERS_H

#include <vector>
#include <set>
#include <algorithm>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include <SDL/SDL_image.h>
#include <Box2D/Box2D.h>
#include "vectors.h"

typedef GLuint TEXTURE;
typedef V4 COLOR;

#ifdef __linux__ 
typedef Uint32 UINT;
#endif

#endif