#ifndef CORE_TEXTUREMANAGER_CPP
#define CORE_TEXTUREMANAGER_CPP

#include "texturemanager.h"

//=========================================================
// TEXTUREMANAGER SINGLETON
//=========================================================
TextureManager& TextureManager::GetInstance()
{
	static TextureManager instance;
	return instance;
}

//=========================================================
// TEXTUREMANAGER INIT
//=========================================================
bool TextureManager::Init()
{
	return true;
}

//=========================================================
// TEXTUREMANAGER LOADTEXTURE
//=========================================================
TEXTURE TextureManager::LoadTexture(char* source, char* name)
{
	return LoadTexture( source, name, GL_LINEAR);
}

TEXTURE TextureManager::LoadTexture(char* source, char* name, GLint type)
{
	GLuint texture;			
	SDL_Surface *surface;	
	GLenum texture_format;
	GLint  nOfColors;
 
	if(!(surface = IMG_Load(source))) return 0; 
  
	nOfColors = surface->format->BytesPerPixel;

    if(nOfColors == 4)  
		 texture_format = surface->format->Rmask == 0x000000ff ? GL_RGBA : GL_BGRA;
    else if(nOfColors == 3)   
		texture_format = surface->format->Rmask == 0x000000ff ? GL_RGB : GL_BGR;
    
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, type);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, type);
 
	glTexImage2D(GL_TEXTURE_2D, 0, nOfColors, surface->w, surface->h, 0,
                      texture_format, GL_UNSIGNED_BYTE, surface->pixels);

	TEXTURENAME tn;
	tn.name = name;
	tn.texture = texture;
	textureList.push_back(tn);
	
	SDL_FreeSurface(surface);

	return texture;
}

//=========================================================
// TEXTUREMANAGER GETTEXTURE
//=========================================================
TEXTURE TextureManager::GetTexture(char* name)
{
	for(std::vector<TEXTURENAME>::iterator it = textureList.begin() ; it != textureList.end(); ++it)
		if(strcmp(name, (*it).name) == 0)
			return (*it).texture;

	return 0;
}

//=========================================================
// TEXTUREMANAGER REMOVETEXTURE
//=========================================================
bool TextureManager::RemoveTexture(char* name)
{
	for(std::vector<TEXTURENAME>::iterator it = textureList.begin() ; it != textureList.end(); ++it)
		if(strcmp(name, (*it).name) == 0)
		{
			glDeleteTextures(1, &(*it).texture);
			textureList.erase(it);
			return true;
		}

	return false;
}

//=========================================================
// TEXTUREMANAGER REMOVEALLTEXTURES
//=========================================================
bool TextureManager::RemoveAll()
{
	for(std::vector<TEXTURENAME>::iterator it = textureList.begin() ; it != textureList.end(); ++it)
		glDeleteTextures(1, &(*it).texture);

	textureList.clear();

	return true;
}

#endif