#ifndef CORE_CAMERA_H
#define CORE_CAMERA_H

#include "headers.h"
#include "settings.h"

//=========================================================
// CAMERA
//=========================================================
// 
//	o - CAM po�o�enie obseratora
//  x - EYE punkt widzenia
//
//    /
//   / 
//	o--x
//   \
//    \
//_________________________________________________________


//=========================================================
// CAMERA SETTINGS
//=========================================================
#define CORE_CAMERA_CAM_SPEED 1
#define CORE_CAMERA_EYE_SPEED 1
#define CORE_CAMERA_WIDTH 0.5
static float CORE_CAMERA_RATIO; //RATIO 16/9  9/16 = 0.5625
//_________________________________________________________

class Camera
{
public:
	static Camera& GetInstance();
	
	V4 screen;

	bool Init();
	void Update(float dt);

	void Set3D();
	void Set2D();

	void SetCamCurrentPosition(V3 pos);
	void SetEyeCurrentPosition(V3 pos);
	void SetRequiredPosition(V3 pos);
	void SetCamSpeed(float speed);
	void SetEyeSpeed(float speed);


	V3 GetCurrentPosition();
	V3 GetRequiredPosition();

private:
	Camera() {};
	~Camera() {};

	V3 camCurrentPosition;
	V3 eyeCurrentPosition;
	V3 requiredPosition;
	GLfloat camSpeed;
	GLfloat eyeSpeed;
};

#endif