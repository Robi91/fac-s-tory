#ifndef CORE_LANGUAGE_CPP
#define CORE_LANGUAGE_CPP

#include "language.h"

//=========================================================
// LANGUAGE SINGLETON
//=========================================================
Language& Language::GetInstance()
{
	static Language instance;
	return instance;
}

void Language::Init(char *src)
{
	FILE * file;
	file = fopen(src, "r");

	if(file != NULL)
	{
		char string[256];
		while(fgets(string,256,file) != NULL)
		{
			std::string line = string;
			list.push_back(line);
		}
		fclose(file);
	}

}

std::string Language::GetString(unsigned int id)
{
	if(list.size() <= id) return "UNDEFINDE";
	return list[id];
}
#endif