#ifndef CORE_GRAPHICS_CPP
#define CORE_GRAPHICS_CPP

#include "graphics.h"

//=========================================================
// GRAPHICS SINGLETON
//=========================================================
Graphics& Graphics::GetInstance()
{
	static Graphics instance;
	return instance;
}

//=========================================================
// GRAPHICS INIT
//=========================================================
bool Graphics::Init()
{
	lastTexture = 0;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
	
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if(glGetError() != GL_NO_ERROR) return false;
	return true;
}

//=========================================================
// GRAPHICS DRAW
//=========================================================
void Graphics::Draw(TEXTURE texture, V3 position, V2 size, V4 uv, GLfloat angle, COLOR color)
{
	GLfloat w = size.x;
	GLfloat h = size.y;
	
	if(lastTexture != texture)
	{
		glBindTexture(GL_TEXTURE_2D, texture);
		lastTexture = texture;
	}

	glPushMatrix();

	glTranslatef(position.x, position.y, position.z);
	glRotatef(angle, 0, 0, 1);

	glColor4f(color.x, color.y, color.z, color.w);
	glBegin(GL_QUADS);
		glTexCoord2f(uv.x,  uv.w);
        glVertex3f(-w, -h, 0);

		glTexCoord2f(uv.z,  uv.w);
        glVertex3f( w, -h, 0);

		glTexCoord2f(uv.z,  uv.y);
        glVertex3f( w,  h, 0);

		glTexCoord2f(uv.x,  uv.y);
        glVertex3f(-w,  h, 0);
	glEnd();

	glPopMatrix();
}

//=========================================================
// GRAPHICS FLUSH
//=========================================================
void Graphics::Flush()
{
	SDL_GL_SwapBuffers();
    glClear( GL_COLOR_BUFFER_BIT);
}

#endif