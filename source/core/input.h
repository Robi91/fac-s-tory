#ifndef CORE_INPUT_H
#define CORE_INPUT_H

#include "headers.h"

class  Input
{
public:
	static Input& GetInstance();

	bool Init();
	bool Update();
	bool KeyPressed(SDLKey key);
	bool KeyTapped(SDLKey key);
	bool MousePressed(char button);
	bool MouseTapped(char button);
	V2 GetMousePosition();

private:
	Input() {};
	~Input() {};

	SDL_Event event;
	Uint8 *keyState;
	Uint8 keyStatePrev[323];
	Uint8 mousePrevState;

	void KeySnapshot();
	void MouseSnapshot();
};

#endif