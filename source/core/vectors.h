#ifndef CORE_VECTORS_H
#define CORE_VECTORS_H

#include <SDL/SDL_opengl.h>

//=========================================================
// STRUKTURA V2
//=========================================================
struct V2
{
	GLfloat x, y;

	V2():x(0.0f),y(0.0f)
	{
	}

	V2(GLfloat x, GLfloat y):x(x),y(y)
	{
	}

//
//	V2 OPERATOR +
//
	friend V2 operator + (const V2 &vector,const V2 &vector2)
	{
		return V2(vector.x + vector2.x, vector.y + vector2.y);
	}

	V2 operator + (V2 vector)
	{
		return V2(x+vector.x, y+vector.y);
	}

	V2 operator + (GLfloat num)
	{
		return V2(x+num, y+num);
	}

//
//	V2 OPERATOR -
//
	friend V2 operator - (const V2 &vector,const V2 &vector2)
	{
		return V2(vector.x - vector2.x, vector.y - vector2.y);
	}

	V2 operator - (V2 vector)
	{
		return V2(x-vector.x, y-vector.y);
	}

	V2 operator - (GLfloat num)
	{
		return V2(x-num, y-num);
	}

//
//	V2 OPERATOR *
//
	friend V2 operator * (const V2 &vector,const V2 &vector2)
	{
		return V2(vector.x * vector2.x, vector.y * vector2.y);
	}

	V2 operator * (V2 vector)
	{
		return V2(x*vector.x, y*vector.y);
	}

	V2 operator * (GLfloat num)
	{
		return V2(x*num, y*num);
	}

//
//	V2 OPERATOR /
//
	friend V2 operator / (const V2 &vector,const V2 &vector2)
	{
		return V2(vector.x / vector2.x, vector.y / vector2.y);
	}

	V2 operator / (V2 vector)
	{
		return V2(x/vector.x, y/vector.y);
	}

	V2 operator / (GLfloat num)
	{
		return V2(x/num, y/num);
	}

//
//	V2 DODATKOWE FUNKCJE
//
	static V2 SetNull()
	{
		return V2(0.0f, 0.0f);
	}

	static V2 SetOne()
	{
		return V2(1.0f, 1.0f);
	}
};

//=========================================================
// STRUKTURA V3
//=========================================================
struct V3
{
	GLfloat x, y, z;

	V3():x(0.0f),y(0.0f),z(0.0f)
	{
	}

	V3(GLfloat x, GLfloat y, GLfloat z):x(x),y(y),z(z)
	{
	}

//
//	V3 OPERATOR +
//
	friend V3 operator + (const V3 &vector,const V3 &vector2)
	{
		return V3(vector.x + vector2.x, vector.y + vector2.y, vector.z + vector2.z);
	}

	V3 operator + (V3 vector)
	{
		return V3(x+vector.x, y+vector.y, z+vector.z);
	}

	V3 operator + (GLfloat num)
	{
		return V3(x+num, y+num, z+num);
	}

//
//	V3 OPERATOR -
//
	friend V3 operator - (const V3 &vector,const V3 &vector2)
	{
		return V3(vector.x - vector2.x, vector.y - vector2.y, vector.z - vector2.z);
	}

	V3 operator - (V3 vector)
	{
		return V3(x-vector.x, y-vector.y, z-vector.z);
	}

	V3 operator - (GLfloat num)
	{
		return V3(x-num, y-num, z-num);
	}

//
//	V3 OPERATOR *
//
	friend V3 operator * (const V3 &vector,const V3 &vector2)
	{
		return V3(vector.x * vector2.x, vector.y * vector2.y, vector.z * vector2.z);
	}

	V3 operator * (V3 vector)
	{
		return V3(x*vector.x, y*vector.y, z*vector.z);
	}

	V3 operator * (GLfloat num)
	{
		return V3(x*num, y*num, z*num);
	}

//
//	V3 OPERATOR /
//
	friend V3 operator / (const V3 &vector,const V3 &vector2)
	{
		return V3(vector.x / vector2.x, vector.y / vector2.y, vector.z / vector2.z);
	}

	V3 operator / (V3 vector)
	{
		return V3(x/vector.x, y/vector.y, z/vector.z);
	}

	V3 operator / (GLfloat num)
	{
		return V3(x/num, y/num, z/num);
	}

//
//	V3 DODATKOWE FUNKCJE
//
	static V3 SetNull()
	{
		return V3(0.0f, 0.0f, 0.0f);
	}

	static V3 SetOne()
	{
		return V3(1.0f, 1.0f, 1.0f);
	}
};

//=========================================================
// STRUKTURA V4
//=========================================================
struct V4
{
	GLfloat x, y, z, w;

	V4():x(0.0f),y(0.0f),z(0.0f),w(0.0f)
	{
	}

	V4(GLfloat x, GLfloat y, GLfloat z, GLfloat w):x(x),y(y),z(z),w(w)
	{
	}

//
//	V4 OPERATOR +
//
	friend V4 operator + (const V4 &vector,const V4 &vector2)
	{
		return V4(vector.x + vector2.x, vector.y + vector2.y, vector.z + vector2.z, vector.w + vector2.w);
	}

	V4 operator + (V4 vector)
	{
		return V4(x+vector.x, y+vector.y, z+vector.z, w+vector.w);
	}

	V4 operator + (GLfloat num)
	{
		return V4(x+num, y+num, z+num, w+num);
	}

//
//	V4 OPERATOR -
//
	friend V4 operator - (const V4 &vector,const V4 &vector2)
	{
		return V4(vector.x - vector2.x, vector.y - vector2.y, vector.z - vector2.z, vector.w - vector2.w);
	}

	V4 operator - (V4 vector)
	{
		return V4(x-vector.x, y-vector.y, z-vector.z, w-vector.w);
	}

	V4 operator - (GLfloat num)
	{
		return V4(x-num, y-num, z-num, w-num);
	}

//
//	V4 OPERATOR *
//
	friend V4 operator * (const V4 &vector,const V4 &vector2)
	{
		return V4(vector.x * vector2.x, vector.y * vector2.y, vector.z * vector2.z, vector.w * vector2.w);
	}

	V4 operator * (V4 vector)
	{
		return V4(x*vector.x, y*vector.y, z*vector.z, w*vector.w);
	}

	V4 operator * (GLfloat num)
	{
		return V4(x*num, y*num, z*num, w*num);
	}

//
//	V4 OPERATOR /
//
	friend V4 operator / (const V4 &vector,const V4 &vector2)
	{
		return V4(vector.x / vector2.x, vector.y / vector2.y, vector.z / vector2.z, vector.w / vector2.w);
	}

	V4 operator / (V4 vector)
	{
		return V4(x/vector.x, y/vector.y, z/vector.z, w/vector.w);
	}

	V4 operator / (GLfloat num)
	{
		return V4(x/num, y/num, z/num, w/num);
	}

//
//	V4 DODATKOWE FUNKCJE
//
	static V4 SetNull()
	{
		return V4(0.0f, 0.0f, 0.0f, 0.0f);
	}

	static V4 SetOne()
	{
		return V4(1.0f, 1.0f, 1.0f, 1.0f);
	}
};

//=========================================================
// GLOBALNE FUNKCJE
//=========================================================
#define PI 3.14159265
struct Func
{

	//
	//	Losobwanie liczb float z przedziału
	//
	static float Rand(float x, float y)
	{
		if(x == y) return x;

		int ix = (int)(x*100);
		int iy = (int)(y*100);
		int r = rand()%(iy - ix);
		return (r + ix) * 0.01f;
		return 1.0f;
	}

	static float DtoR(float r)
	{
		return (float)(r * PI / 180.0f);
	}

	static float RtoD(float a)
	{
		return  (float)(a * 180.0f / PI);
	}
};

#endif