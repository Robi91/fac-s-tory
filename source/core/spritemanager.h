#ifndef CORE_SPRITEMANAGER_H
#define CORE_SPRITEMANAGER_H

#include "headers.h"
#include "sprite.h"

class  SpriteManager
{
public:
	static SpriteManager& GetInstance();

	bool Init();
	void Add(Sprite* object);
	bool Remove(Sprite* object);
	void RemoveAll();
	void Add2D(Sprite* object);
	bool Remove2D(Sprite* object);
	void RemoveAll2D();
	void Render();
	void Render2D();
	void Update(float dt);

private:
	SpriteManager() {};
	~SpriteManager() {};

	void Sort();
	static bool SortFunc(Sprite* object1, Sprite* object2);

	std::vector<Sprite*> spriteList;
	std::vector<Sprite*> sprite2DList;
};

#endif