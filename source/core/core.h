#ifndef CORE_H
#define CORE_H

#include "headers.h"
#include "vectors.h"
#include "graphics.h"
#include "texturemanager.h"
#include "input.h"
#include "camera.h"
#include "sprite.h"
#include "particles.h"
#include "spritemanager.h"
#include "physicsmanager.h"
#include "language.h"
#include "settings.h"

#endif 