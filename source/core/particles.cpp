#ifndef CORE_PARTICLES_CPP
#define CORE_PARTICLES_CPP

#include "particles.h"

//=========================================================
// PARTICLES KONSTRUKTOR
//=========================================================
Particles::Particles()
{
	maxPartices = 15;
	speed = V2(0.1f, 1.0f);
	zspeed = V2(0.0f, 0.0f);
	life = V2(1.0f, 2.0f);
	exitAngle = V2(0.0f,0.0f);
	startColor = COLOR::SetOne();
	endColor = COLOR(1.0f, 1.0f, 1.0f, 0.0f);
	particleSize = V4(0.1f, 0.1f, 0.2f, 0.2f);
	time = 0.0f;
	lastCreate = 0.0f;
	interval = 0.1f;
	pause = false;
	respawn = true;
	ratio = 1.0f;
}

//=========================================================
// PARTICLES RYSOWANIE
//=========================================================
void Particles::Render()
{
	for(std::vector<PARTICLE>::reverse_iterator it = particleList.rbegin() ; it != particleList.rend(); ++it)
	{
		Graphics::GetInstance().Draw(texture, (*it).position, (*it).size, uv, (*it).angle, (*it).color); 
	}
}

//=========================================================
// PARTICLES AKTUALIZACJA
//=========================================================
void Particles::Update(float dt)
{
	if(pause)return;
	time += dt;

	for(std::vector<PARTICLE>::iterator it = particleList.begin() ; it != particleList.end(); )
	{
		
		if((*it).life < time)
		{
			it = particleList.erase(it);
		}else{
			(*it).position = (*it).position + (*it).force * dt;
			float totalTime = (*it).life - (*it).startTime;
			float currentTime = time - (*it).startTime;
			float percent = currentTime/totalTime;
			(*it).color = startColor * (1-percent) + endColor * percent;
			++it;
		}
	}
	
	if(!respawn) return;
	if(particleList.size() < maxPartices)
	{
		if(time - lastCreate > interval)
		{
			if(rand()%2 == 0)
			{
				CreateParticle();
			}
		}
	}
	
	
}

//=========================================================
// PARTICLES USUWANIE
//=========================================================
void Particles::Remove()
{
	particleList.clear();
}

//=========================================================
// PARTICLES TWORZENIE POJEDYUNCZEJ CZASTECZKI
//=========================================================
void Particles::CreateParticle()
{
	PARTICLE p;
	p.position.x = Func::Rand(-size.x, size.x) + position.x;
	p.position.y = Func::Rand(-size.y, size.y) + position.y;
	p.position.z = position.z;	
	
	if(ratio == 0)
	{
		p.size.x = Func::Rand(particleSize.x, particleSize.z);
		p.size.y = Func::Rand(particleSize.y, particleSize.w);
	}else{
		p.size.x = Func::Rand(particleSize.x, particleSize.z);
		p.size.y = ratio * p.size.x;
	}

	p.life = time + Func::Rand(life.x, life.y);
	p.startTime = time;
	
	
	float a = Func::Rand(exitAngle.x, exitAngle.y);
	float s = Func::Rand(speed.x, speed.y);
	
	p.angle = a;
	
	p.force.x = cos(Func::DtoR(a)) * s; 
	p.force.y = sin(Func::DtoR(a)) * s;
	p.force.z = Func::Rand(zspeed.x, zspeed.y);

	p.color = startColor;
	
	lastCreate = time;

	particleList.push_back(p);
}

//=========================================================
// PARTICLES PLAY
//=========================================================
void Particles::Play()
{
	pause = false;
}

//=========================================================
// PARTICLES PAUSE
//=========================================================
void Particles::Pause()
{
	pause = true;
}

//=========================================================
// PARTICLES STOP RESPAWN
//=========================================================
void Particles::StopRespawn()
{
	respawn = false;
}

//=========================================================
// PARTICLES PLAY RESPAWN
//=========================================================
void Particles::PlayRespawn()
{
	respawn = true;
}


#endif