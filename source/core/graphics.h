#ifndef CORE_GRAPHICS_H
#define CORE_GRAPHICS_H

#include "headers.h"

class  Graphics
{
public:
	static Graphics& GetInstance();

	bool Init();
	void Draw(TEXTURE texture, V3 position, V2 size, V4 uv, GLfloat angle, COLOR color);
	void Flush();

private:
	Graphics() {};
	~Graphics() {};

	TEXTURE lastTexture; //Ostatnio renderowana tekstura
};

#endif