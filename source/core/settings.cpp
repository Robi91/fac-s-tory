#ifndef CORE_SETTINGS_CPP
#define CORE_SETTINGS_CPP

#include "settings.h"

Settings& Settings::GetInstance()
{
	static Settings instance;
	return instance;
}

std::string Settings::GetString(std::string _setting)
{
    bool found = false;
    int i = 0;
    while(i<size && !found)
    {
        if(settings_array[i].key!=_setting)
            i++;
        else
            found = true;
    }
    if(found)
        return settings_array[i].value;
    else
        return "";
}

bool Settings::GetBool(std::string _setting)
{
    if(Settings::GetString(_setting).substr(0,4)=="true")
        return true;
    else
        return false;
}

int Settings::GetInt(std::string _setting)
{
	return strtol(Settings::GetString(_setting).c_str(), NULL, 10);
}

float Settings::GetFloat(std::string _setting)
{
	return strtof(Settings::GetString(_setting).c_str(), NULL);
}

void Settings::Init(char *src)
{
    size = 0;
    FILE * file;
    file = fopen(src, "r");

    if(file != NULL)
    {
        char str[256];
        while(fgets(str,256,file) != NULL)
            size++;

        settings_array = new SETTING[size];

        fseek(file,0,SEEK_SET);

        std::string tmp;
        for(int i=0; i<size; i++)
        {
            fgets(str,256,file);
			tmp = str;
			
            settings_array[i].key=tmp.substr(0,tmp.find('='));
            settings_array[i].value=tmp.substr(tmp.find('=')+1);
		}

		fclose(file);
    }

}

Settings::~Settings()
{
    delete [] settings_array;
}

#endif
