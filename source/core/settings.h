#ifndef CORE_SETTINGS_H
#define CORE_SETTINGS_H

#include "headers.h"

struct SETTING
{
    std::string key;
    std::string value;
};

class Settings
{
private:
    int size;
    SETTING *settings_array;

public:
    static Settings& GetInstance();
    std::string GetString(std::string _setting);
    bool GetBool(std::string _setting);
    int GetInt(std::string _setting);
    float GetFloat(std::string _setting);
    void Init(char *file);
    Settings(){};
    ~Settings();
};

#endif
