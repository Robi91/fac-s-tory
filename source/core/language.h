#ifndef CORE_LANGUAGE_H
#define CORE_LANGUAGE_H

#include "headers.h"

//=========================================================
// LANGUAGE
//=========================================================


class  Language
{
public:
	static Language& GetInstance();

	void Init(char *file);
	std::string GetString(unsigned int id);

private:
	Language() {};
	~Language() {};

	std::vector<std::string> list;
};

#endif