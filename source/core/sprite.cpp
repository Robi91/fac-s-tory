#ifndef CORE_SPRITE_CPP
#define CORE_SPRITE_CPP

#include "sprite.h"

//=========================================================
// SPRITE KONSTRUKTOR
//=========================================================
Sprite::Sprite()
{
	position = V3();
	size = V2::SetOne();
	uv = V4(0,0,1.0f, 1.0f);
	texture = 0;
	angle = 0;
	color = COLOR::SetOne();
	hidden = false;
}

//=========================================================
// SPRITE RYSOWANIE
//=========================================================
void Sprite::Render()
{
	if(hidden == true) return;
	Graphics::GetInstance().Draw(texture, position, size, uv, angle, color);
}

//=========================================================
// SPRITESHEETANIMATION KONSTRUKTOR
//=========================================================
SpriteSheetAnimation::SpriteSheetAnimation()
{
	time = 0;
	frameTime = 100;
	pause = false;
	loop = true;
}

//=========================================================
// SPRITESHEETANIMATION WYŚWIETLANIE
//=========================================================
void SpriteSheetAnimation::Render()
{
	if(hidden == true) return;
	Graphics::GetInstance().Draw(texture, position, size, (*nowFrame), angle, color);
}

//=========================================================
// SPRITESHEETANIMATION AKTUALIZACJA STANOW
//=========================================================
void SpriteSheetAnimation::Update(float dt)
{
	if(pause) return;
	time += (int)(dt*1000);

	UINT f = time / (UINT)(frameTime*1000);
	UINT r = time % (UINT)(frameTime*1000);

	nowFrame += f % frames.size();
	time = r;

	if(nowFrame == frames.end())
	{	
		nowFrame = frames.begin();
		if(loop == false) Pause();
	}
}

//=========================================================
// SPRITESHEETANIMATION USUWANIE
//=========================================================
void SpriteSheetAnimation::Remove()
{
	frames.clear();
}

//=========================================================
// SPRITESHEETANIMATION DODAWANIE KLATKI
//=========================================================
void SpriteSheetAnimation::Add(V4 uv)
{
	frames.push_back(uv);
	nowFrame = frames.begin();
}

//=========================================================
// SPRITESHEETANIMATION PLAY
//=========================================================
void SpriteSheetAnimation::Play()
{
	pause = false;
}

//=========================================================
// SPRITESHEETANIMATION PAUSE
//=========================================================
void SpriteSheetAnimation::Pause()
{
	pause = true;
}

//=========================================================
// SPRITESHEETANIMATION STOP
//=========================================================
void SpriteSheetAnimation::Stop()
{
	pause = true;
	nowFrame = frames.begin();
	time = 0;
}

//=========================================================
// SPRITEASANIMATION KONSTUKTOR
//=========================================================
SpriteASAnimation::SpriteASAnimation()
{
	loop = true;
	time = 0;
	pause = false;
	current = ASANIMATION();
}

//=========================================================
// SPRITEASANIMATION RENDER
//=========================================================
void SpriteASAnimation::Render()
{
	if(hidden == true) return;
	Graphics::GetInstance().Draw(texture, position + current.position, size + current.size, uv + current.uv, angle + current.angle, current.color);
}

//=========================================================
// SPRITEASANIMATION AKTUALIZACJA
//=========================================================
void SpriteASAnimation::Update(float dt)
{
	if(pause) return;
	time += dt;

	float currentTime = time - (*prevStep).time;
	float totalTime = (*currentStep).time - (*prevStep).time;
	float percent = currentTime / totalTime;
	
	current.position = (*prevStep).position + (((*currentStep).position - (*prevStep).position) * percent);
	current.color = (*prevStep).color + (((*currentStep).color - (*prevStep).color) * percent);
	current.angle = (*prevStep).angle + (((*currentStep).angle - (*prevStep).angle) * percent);
	current.size = (*prevStep).size + (((*currentStep).size - (*prevStep).size) * percent);
	current.uv = (*prevStep).uv + (((*currentStep).uv - (*prevStep).uv) * percent);

	while((*currentStep).time < time)
	{
		prevStep = currentStep;
		currentStep ++;

		if(currentStep == steps.end())
		{
			currentStep = steps.begin();
			double x = time / (*prevStep).time;
			double c;
			modf(x,&c);
			time = time - (*prevStep).time * (float)c;
			
			if(!loop)
			{
				Pause();
			}
		}
	}
}

//=========================================================
// SPRITEASANIMATION USUWANIE
//=========================================================
void SpriteASAnimation::Remove()
{
	steps.clear();
}


//=========================================================
// SPRITEASANIMATION AKTUALIZACJA
//=========================================================
void SpriteASAnimation::Add(ASANIMATION as)
{
	steps.insert(as);
	currentStep = steps.begin();
	prevStep = steps.end();
	prevStep --;
}

//=========================================================
// SPRITEASANIMATION PLAY
//=========================================================
void SpriteASAnimation::Play()
{
	pause = false;
}

//=========================================================
// SPRITEASANIMATION PAUSE
//=========================================================
void SpriteASAnimation::Pause()
{
	pause = true;
}

//=========================================================
// SPRITEASANIMATION STOP
//=========================================================
void SpriteASAnimation::Stop()
{
	currentStep = steps.begin();
	prevStep = steps.end();
	prevStep --;
	time = 0;
	Update(0);
	pause = true;
}

//=========================================================
// TEXT CONSTRUCTOR
//=========================================================
Text::Text() : Sprite()
{
	text = "";
	offset = 0.1f;
}

//=========================================================
// TEXT REBDER
//=========================================================
#define FONT_SIZE 16
#define FONT_SIZE_W 0.0517578125f
#define FONT_SIZE_H 0.08203125f
#define FONT_OFFSET 2
#define FONT_RATIO 0.630952380952381f

void Text::Render()
{
	if(hidden == true) return;
	for(unsigned int i = 0; i < text.length(); i++)
	{
		V3 pos = position;
		pos.x += i*size.x;
		
		char z = text[i];

		V4 uv;
		uv.x = (((int)z) % FONT_SIZE) * FONT_SIZE_W;
		uv.y = (((int)z) / FONT_SIZE - FONT_OFFSET) * FONT_SIZE_H;
		uv.z = uv.x + FONT_SIZE_W;
		uv.w = uv.y + FONT_SIZE_H;

		size.x = size.y * FONT_RATIO;

		Graphics::GetInstance().Draw(texture, pos, size, uv, angle, color);
	}
}



#endif