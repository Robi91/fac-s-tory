#ifndef CORE_PARTICLES_H
#define CORE_PARTICLES_H

#include "headers.h"
#include "sprite.h"


//=========================================================
// PARTICLE
//=========================================================
struct PARTICLE
{
	V3 position;
	V3 force;
	V2 size;
	GLfloat angle;
	float startTime;
	float life;
	COLOR color;

	PARTICLE() : position(V3()), size(V2(0.1f, 0.1f)), angle(0.0f), life(1.0f), color(COLOR::SetOne()), force(V3(0.001f,0,0))
	{
	}
};

//=========================================================
// PARTICLES
//=========================================================
class  Particles : public Sprite
{
public:
	std::vector<PARTICLE> particleList;
	UINT maxPartices;
	V2 exitAngle;
	V2 speed;
	V2 zspeed;
	V2 life;
	COLOR startColor;
	COLOR endColor;
	V4 particleSize;
	float interval;
	float ratio;

	Particles();

	void Render();
	void Update(float dt);
	void Remove();

	void Pause();
	void Play();
	void StopRespawn();
	void PlayRespawn();

private:
	bool pause;
	bool respawn;
	float time;
	float lastCreate;

	void CreateParticle();
};

#endif