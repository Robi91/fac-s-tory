#ifndef CORE_SPRITE_H
#define CORE_SPRITE_H

#include "headers.h"
#include "graphics.h"


//=========================================================
// SPRITE
//=========================================================
class  Sprite
{
public:
	V3 position;
	V2 size;
	V4 uv;
	TEXTURE texture;
	GLfloat angle;
	COLOR color;
	bool hidden;

	Sprite();

	virtual void Render();
	virtual void Update(float dt) {};
	virtual void Remove(){};
};


//=========================================================
// SPRITESHEETANIMATION
//=========================================================
class  SpriteSheetAnimation : public Sprite
{
public:
	std::vector<V4> frames;
	float frameTime;
	bool loop;
	
	SpriteSheetAnimation();
	void Render();
	void Update(float dt);
	void Remove();
	void Add(V4 uv);
	void Play();
	void Pause();
	void Stop();

protected:
	std::vector<V4>::iterator nowFrame;
	UINT time;
	bool pause;

};


//=========================================================
// ASSETSHEETANIMATION
//=========================================================
struct ASANIMATION
{
	V3 position;
	V2 size;
	V4 uv;
	GLfloat angle;
	COLOR color;
	float time;

	ASANIMATION(): position(V3()), size(V2()), uv(V4(0,0,0,0)), angle(0.0f), color(COLOR::SetOne()), time(0.0f)
	{
	}

	friend bool operator< (ASANIMATION const&o1, ASANIMATION const&o2)
	{
		return o1.time < o2.time;
	}
};


//=========================================================
// SPRITEASSETSHEETANIMATION 
//=========================================================
class  SpriteASAnimation : public Sprite
{
public:
	std::set<ASANIMATION> steps;
	bool loop;
	
	SpriteASAnimation();
	void Render();
	void Update(float dt);
	void Remove();
	void Add(ASANIMATION as);
	void Play();
	void Pause();
	void Stop();

protected:
	std::set<ASANIMATION>::iterator currentStep;
	std::set<ASANIMATION>::iterator prevStep;
	ASANIMATION current;
	float time;
	bool pause;
};


//=========================================================
// TEXT
//=========================================================
class  Text : public Sprite
{
public:
	std::string text;
	float offset;

	Text();

	virtual void Render();
	virtual void Update(float dt) {};
	virtual void Remove(){};
};

#endif