#ifndef CORE_TEXTUREMANAGER_H
#define CORE_TEXTUREMANAGER_H

#include "headers.h"

struct TEXTURENAME
{
	TEXTURE texture;
	char* name;
};

class  TextureManager
{
public:
	static TextureManager& GetInstance();
	bool Init();
	TEXTURE LoadTexture(char* source, char* name);
	TEXTURE LoadTexture(char* source, char* name, GLint type);
	TEXTURE GetTexture(char* name);
	bool RemoveTexture(char* name);
	bool RemoveAll();

private:
	TextureManager() {};
	~TextureManager() {};

	std::vector<TEXTURENAME> textureList;
};

#endif