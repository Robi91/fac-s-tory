#ifndef CORE_CAMERA_CPP
#define CORE_CAMERA_CPP

#include "camera.h"

//=========================================================
// CAMERA SINGLETON
//=========================================================
Camera& Camera::GetInstance()
{
	static Camera instance;
	return instance;
}

//=========================================================
// CAMERA INIT
//=========================================================
bool Camera::Init()
{
	CORE_CAMERA_RATIO = Settings::GetInstance().GetFloat("height") / Settings::GetInstance().GetFloat("width");
	screen = V4(-2,2,-2,2);
	camCurrentPosition = V3();
	eyeCurrentPosition = V3();
	requiredPosition = V3();
	camSpeed = CORE_CAMERA_CAM_SPEED;
	eyeSpeed = CORE_CAMERA_EYE_SPEED;

	Set3D();

	Update(1);

	return true;
}

//=========================================================
// CAMERA 3D
//=========================================================
void Camera::Set3D()
{
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

	glFrustum(-CORE_CAMERA_WIDTH, CORE_CAMERA_WIDTH,
			  -CORE_CAMERA_WIDTH * CORE_CAMERA_RATIO, CORE_CAMERA_WIDTH * CORE_CAMERA_RATIO,
			  10, 600 );
}

//=========================================================
// CAMERA 2D
//=========================================================
void Camera::Set2D()
{
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluOrtho2D(-1.0f, 1.0f, -1.0f * CORE_CAMERA_RATIO, 1.0f * CORE_CAMERA_RATIO);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(0.375, 0.375, 0.0);
}

//=========================================================
// CAMERA UPDATE
//=========================================================
void Camera::Update(float dt)
{
	//Obliczanie nowej pozycji kamery
	camCurrentPosition = camCurrentPosition + ((requiredPosition - camCurrentPosition) * camSpeed * dt);
	eyeCurrentPosition = eyeCurrentPosition + ((requiredPosition - eyeCurrentPosition) * eyeSpeed * dt);

	camCurrentPosition.z = eyeCurrentPosition.z+35;
	eyeCurrentPosition.z = eyeCurrentPosition.z;

	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	gluLookAt(camCurrentPosition.x, camCurrentPosition.y, camCurrentPosition.z,
			  eyeCurrentPosition.x, eyeCurrentPosition.y, eyeCurrentPosition.z,
			  0.0, 1.0f, 0.0 );
}

//=========================================================
// CAMERA SET CAM
//=========================================================
void Camera::SetCamCurrentPosition(V3 pos)
{
	camCurrentPosition = pos;
}

//=========================================================
// CAMERA SET EYE
//=========================================================
void Camera::SetEyeCurrentPosition(V3 pos)
{
	eyeCurrentPosition = pos;
}

//=========================================================
// CAMERA SET REQUIRE
//=========================================================
void Camera::SetRequiredPosition(V3 pos)
{
	if(pos.x < screen.x) pos.x = screen.x;
	if(pos.x > screen.y) pos.x = screen.y;
	if(pos.y < screen.z) pos.y = screen.z;
	if(pos.y > screen.w) pos.y = screen.w;
	requiredPosition = pos;
}

//=========================================================
// CAMERA SET CAM SPEED
//=========================================================
void Camera::SetCamSpeed(float speed)
{
	camSpeed = speed;
}

//=========================================================
// CAMERA SET EYE SPEED
//=========================================================
void Camera::SetEyeSpeed(float speed)
{
	eyeSpeed = speed;
}

//=========================================================
// CAMERA GET CURRENT POSITION
//=========================================================
V3 Camera::GetCurrentPosition()
{
	return camCurrentPosition;
}

//=========================================================
// CAMERA GET REQUIRED POSITION
//=========================================================
V3 Camera::GetRequiredPosition()
{
	return requiredPosition;
}

#endif