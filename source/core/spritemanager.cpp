#ifndef CORE_SPRITEMANAGER_CPP
#define CORE_SPRITEMANAGER_CPP

#include "spritemanager.h"

//=========================================================
// SPRITEMANAGER SINGLETON
//=========================================================
SpriteManager& SpriteManager::GetInstance()
{
	static SpriteManager instance;
	return instance;
}

//=========================================================
// SPRITEMANAGER INIT
//=========================================================
bool SpriteManager::Init()
{
	return true;
}

//=========================================================
// SPRITEMANAGER ADD
//=========================================================
void SpriteManager::Add(Sprite* object)
{
	spriteList.push_back(object);
}

//=========================================================
// SPRITEMANAGER REMOVE
//=========================================================
bool SpriteManager::Remove(Sprite* object)
{
	for(std::vector<Sprite*>::iterator it = spriteList.begin() ; it != spriteList.end(); ++it)
		if((*it) == object)
		{
			(*it)->Remove();
			delete (*it);
			spriteList.erase(it);
			return true;
		}

	return false;
}

//=========================================================
// SPRITEMANAGER REMOVE ALL
//=========================================================
void SpriteManager::RemoveAll()
{
	for(std::vector<Sprite*>::iterator it = spriteList.begin() ; it != spriteList.end(); ++it)
		delete (*it);

	spriteList.clear();
}

//=========================================================
// SPRITEMANAGER ADD2D
//=========================================================
void SpriteManager::Add2D(Sprite* object)
{
	sprite2DList.push_back(object);
}

//=========================================================
// SPRITEMANAGER REMOVE2D
//=========================================================
bool SpriteManager::Remove2D(Sprite* object)
{
	for(std::vector<Sprite*>::iterator it = sprite2DList.begin() ; it != sprite2DList.end(); ++it)
		if((*it) == object)
		{
			(*it)->Remove();
			delete (*it);
			sprite2DList.erase(it);
			return true;
		}

	return false;
}

//=========================================================
// SPRITEMANAGER REMOVE ALL2D
//=========================================================
void SpriteManager::RemoveAll2D()
{
	for(std::vector<Sprite*>::iterator it = sprite2DList.begin() ; it != sprite2DList.end(); ++it)
		delete (*it);

	sprite2DList.clear();
}

//=========================================================
// SPRITEMANAGER RENDER
//=========================================================
void SpriteManager::Render()
{
	Sort();
	for(std::vector<Sprite*>::iterator it = spriteList.begin() ; it != spriteList.end(); ++it)
		(*it)->Render();
}

//=========================================================
// SPRITEMANAGER RENDER2D
//=========================================================
void SpriteManager::Render2D()
{
	Sort();
	for(std::vector<Sprite*>::iterator it = sprite2DList.begin() ; it != sprite2DList.end(); ++it)
		(*it)->Render();
}
//=========================================================
// SPRITEMANAGER UPDATE
//=========================================================
void SpriteManager::Update(float dt)
{
	for(std::vector<Sprite*>::iterator it = spriteList.begin() ; it != spriteList.end(); ++it)
		(*it)->Update(dt);
}

//=========================================================
// SPRITEMANAGER SORT
//=========================================================
void SpriteManager::Sort()
{
	std::sort(spriteList.begin(), spriteList.end(), SpriteManager::SortFunc);
}

//=========================================================
// SPRITEMANAGER SORT FUNCTION
//=========================================================
bool SpriteManager::SortFunc(Sprite* object1, Sprite* object2)
{
	return (object1->position.z < object2->position.z);
}

#endif