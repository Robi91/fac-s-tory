#ifndef CORE_PHYSICSMANAGER_H
#define CORE_PHYSICSMANAGER_H

#include "headers.h"

class  PhysicsManager
{
public:
	b2World* world;
	static PhysicsManager& GetInstance();

private:
	PhysicsManager() {};
	~PhysicsManager() {};
};

#endif